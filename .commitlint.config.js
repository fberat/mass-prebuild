module.exports = {
	extends: ['@commitlint/config-conventional'],

	rules: {
		'subject-case': [
			2,
			'always',
			['sentence-case', 'start-case'],
		],
		'type-enum': [
			2,
			'always',
			[
				'build',
				'chore',
				'ci',
				'docs',
				'feat',
				'fix',
				'perf',
				'refactor',
				'revert',
				'style',
				'test',
				'release',
			],
		],
	}
}
