echo -e "${BBlue}##### Checking main CLI #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

rm -rf ${EXCEPTION_LOG}

echo -e "${Blue}Empty config test${Color_Off}"
cat > ${CONFIG_FILE} << EOF
verbose: 5
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 && XFAIL
res=$(grep "Configuration is broken" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Non-existent build ID test${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: 999999999
clean: True
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 && XFAIL
res=$(grep "Build ID doesn't match any known ID" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Invalid arch${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: blabla
packages: autoconf
revdeps:
  list: automake
backend: dummy
verbose: 5
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 && XFAIL
res=$(grep "is not a supported architecture" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Invalid back-end${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages: autoconf
revdeps:
  list: automake
verbose: 5
backend: blabla
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 && XFAIL
res=$(grep "Unknown back-end" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/o rev deps calculation)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages: autoconf
revdeps:
  list:
    automake:
      deps_only: True
retry: 3
verbose: 5
backend: dummy
name: to_clean
EOF

# May fail on stage 3, but that doesn't matter at this point
timeout 10m mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || true
res=$(grep "backend: dummy" ${LOG_FILE} || true)
res1=$(grep "Executing stage 4" ${LOG_FILE} || true)

ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

if [ "x${res}" == "x" ] || [ "x${res1}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Re-use backend information from database${Color_Off}"
mpb --buildid ${ID} -VVV > ${LOG_FILE} 2>&1 || true
res=$(grep "Using dummy back-end" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Cancel existing${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
cancel: True
backend: dummy
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || FAIL
res=$(grep "Canceling ${ID}" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Clean existing${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
clean: True
backend: dummy
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || FAIL
res=$(grep "Cleaning build ${ID}" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Clean database${Color_Off}"
before_clean=$(mpb --list -VV | grep "to_clean" || true)
mpb --clean-db > ${LOG_FILE} 2>&1 || FAIL
after_clean=$(mpb --list -VV | grep "to_clean" || true)

if [ "x${before_clean}" == "x" ] || [ ! "x${after_clean}" == "x" ]
then
  mpb --list -VV
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ rev deps calculation)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  rubygem-pry:
    committish: '@last_build'
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..10}
do
  timeout 20m mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || true
  res=$(grep "Calculating reverse dependencies" ${LOG_FILE} || true)
  res1=$(grep "backend: dummy" ${LOG_FILE} || true)
  res2=$(grep "Executing stage 4" ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ group)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  '@3D Printing':
    committish: '@last_build'
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..20}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || true
  res=$(grep "Calculating reverse dependencies" ${LOG_FILE} || true)
  res1=$(grep "backend: dummy" ${LOG_FILE} || true)
  res2=$(grep "Executing stage 4" ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ rev deps calculation, append group)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages: rubygem-pry
revdeps:
  append:
    '@3D Printing':
      committish: '@last_build'
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..10}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || true
  res=$(grep "Calculating reverse dependencies" ${LOG_FILE} || true)
  res1=$(grep "backend: dummy" ${LOG_FILE} || true)
  res2=$(grep "Executing stage 4" ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ rev deps calculation, deps-only packages)${Color_Off}"
# The dummy test verifies that the dependency list contains deps from
# autocon-archive, copr tests will check if this package is actually NOT built
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  rubygem-pry:
    deps_only: True
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..10}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || true
  res=$(grep "alexandria" ${LOG_FILE} || true)
  res1=$(grep "backend: dummy" ${LOG_FILE} || true)
  res2=$(grep "Executing stage 4" ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ src_type=file, no file)${Color_Off}"
rm -rf ${SRC_RPM}
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  pkg1:
    src_type: file
    src: pkg.src.rpm
revdeps:
  list:
    pkg2:
      src_type: file
      src: pkg.src.rpm
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..10}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || true
  res=$(grep "file doesn't exists" ${LOG_FILE} || true)
  res1=$(grep "Configuration is broken." ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ src_type=file)${Color_Off}"
touch ${SRC_RPM}
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  pkg1:
    src_type: file
    src: ${SRC_RPM}
revdeps:
  list:
    '@3D Printing':
      src_type: file
      src: ${SRC_RPM}
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..10}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || true
  res=$(grep "Executing stage 4" ${LOG_FILE} || true)
  res1=$(grep "backend: dummy" ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ rev deps calculation, append existing)${Color_Off}"
touch ${SRC_RPM}
mpb-whatrequires rubygem-pry --no-priority -o ${LOG_FILE}
pkg_name=$(grep -E -m 1 "^[a-z].*:" ${LOG_FILE} | sed 's/://' || true)
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  rubygem-pry:
    deps_only: True
revdeps:
  append:
    ${pkg_name}:
      src_type: file
      src: ${SRC_RPM}
verbose: 5
backend: dummy
EOF

# May fail on stage 3, but that doesn't matter at this point
timeout 10m mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || true
res=$(grep "backend: dummy" ${LOG_FILE} || true)
res1=$(grep "Executing stage 4" ${LOG_FILE} || true)

ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

if [ "x${res}" == "x" ] || [ "x${res1}" == "x" ]
then
  FAIL
fi

res=$(mpb --list-packages ${ID} 2>&1 | grep ${pkg_name} | wc -l || true)

if [ ${res} -gt 1 ]
then
  mpb --list-packages ${ID}
  FAIL
fi

res=$(mpb --list-packages ${ID} 2>&1 | grep -A 5 ${pkg_name} | grep "src_type: file" || true)

if [ "x${res}" == "x" ]
then
  mpb --list-packages ${ID}
  FAIL
fi

echo -e "${Blue}Dummy back-end, sample packages${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  rubygem-pry:
    deps_only: True
revdeps:
  sample: 5
retry: 0
verbose: 5
backend: dummy
EOF

timeout 2m mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 || true
res=$(grep "backend: dummy" ${LOG_FILE} || true)
res1=$(grep "Executing stage 3" ${LOG_FILE} || true)

ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

if [ "x${res}" == "x" ] || [ "x${res1}" == "x" ]
then
  FAIL
fi

count=$(mpb --list-packages -b ${ID} 2>&1 | grep -vE "committish|src|src_type|packages|revdeps|list|location| rubygem-pry:" | wc -l || true)

if [ ! $count -eq 5 ]
then
  mpb --list-packages -b ${ID}
  FAIL
fi

if [ -f ${EXCEPTION_LOG} ]
then
  except=$(grep "exception" ${EXCEPTION_LOG} || true)
  if [ ! "x${except}" == "x" ]
  then
    echo -e "${BRed}Exception raised during tests${Color_Off}"
    FAIL
  fi
fi

rm -f ${LOG_FILE} || true
rm -f ${EXCEPTION_LOG} || true

echo -e "${BGreen}OK${Color_Off}"

echo ""
