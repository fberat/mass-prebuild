echo -e "${BBlue}##### Check reverse dependency calculation #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

EXCEPTION_LOG="${HOME}/.mpb/mpb-whatrequires.log"

for i in {4..5}
do
  t_count=1
  echo -e "${BYellow}Now working with DNF${i}${Color_Off}"
  if [ ${i} -eq 5 ]
  then
    ${SUDO} dnf install -y python3-libdnf5
  fi

  rm -rf ${EXCEPTION_LOG}
  echo -e "${Blue}Retrieve redhat-rpm-config reverse dependencies${Color_Off}"

  mpb-whatrequires --verbose --output ${LOG_FILE} redhat-rpm-config

  if ! grep -qE "Using DNF${i} API" ${EXCEPTION_LOG}
  then
    cat ${}
    FAIL
  fi

  if [ $(cat ${LOG_FILE} | wc -l) -eq 0 ]
  then
    FAIL
  fi

  echo -e "${Blue}Look for rpm in reverse dependencies${Color_Off}"

  if ! grep -qE '^rpm' ${LOG_FILE}
  then
    FAIL
  fi

  mv ${LOG_FILE}{,.${t_count}.dnf${i}}
  t_count=$((t_count + 1))

  echo -e "${Blue}Check for support of pkgconfig(foo) in reverse dependencies${Color_Off}"

  mpb-whatrequires --verbose --output ${LOG_FILE} "pkgconfig(libffi)"

  if ! grep -qE 'gobject-introspection:' ${LOG_FILE}
  then
    FAIL
  fi

  echo -e "${Blue}Check that we don't have devel packages in reverse dependencies${Color_Off}"

  if grep -qE '^[a-zA-Z0-9_-]*-devel:' ${LOG_FILE}
  then
    FAIL
  fi

  mv ${LOG_FILE}{,.${t_count}.dnf${i}}
  t_count=$((t_count + 1))

  echo -e "${Blue}Check for support of rubygem(foo) in reverse dependencies${Color_Off}"

  mpb-whatrequires --verbose --output ${LOG_FILE} "rubygem(pry)"

  if ! grep -qE 'alexandria:' ${LOG_FILE}
  then
    FAIL
  fi

  mv ${LOG_FILE}{,.${t_count}.dnf${i}}
  t_count=$((t_count + 1))

  echo -e "${Blue}Check for support of 'Core' group by group name${Color_Off}"

  mpb-whatrequires --verbose -a 'x86_64' 'Core' --no-deps > ${LOG_FILE} 2>&1

  if ! grep -qE '^\(x86_64\) coreutils:' ${LOG_FILE}
  then
    FAIL
  fi

  if ! grep -qE '^\(x86_64\) initscripts:' ${LOG_FILE}
  then
    FAIL
  fi

  mv ${LOG_FILE}{,.${t_count}.dnf${i}}
  t_count=$((t_count + 1))

  echo -e "${Blue}Check for support of 'Core' group by group ID${Color_Off}"

  mpb-whatrequires --verbose -a 'x86_64' 'core' --no-deps > ${LOG_FILE} 2>&1

  if ! grep -qE '^\(x86_64\) coreutils:' ${LOG_FILE}
  then
    FAIL
  fi

  if ! grep -qE '^\(x86_64\) initscripts:' ${LOG_FILE}
  then
    FAIL
  fi

  mv ${LOG_FILE}{,.${t_count}.dnf${i}}
  t_count=$((t_count + 1))

  echo -e "${Blue}Check for support of 'Core' group in reverse dependencies (multi-arch)${Color_Off}"

  mpb-whatrequires --verbose -a 'x86_64 aarch64' 'Core' --no-deps > ${LOG_FILE} 2>&1

  if ! grep -qE '^\(aarch64\) coreutils:' ${LOG_FILE}
  then
    FAIL
  fi

  if ! grep -qE '^\(x86_64\) coreutils:' ${LOG_FILE}
  then
    FAIL
  fi

  mv ${LOG_FILE}{,.${t_count}.dnf${i}}
  t_count=$((t_count + 1))

  echo -e "${Blue}Retrieve redhat-rpm-config reverse dependencies (c9s)${Color_Off}"

  mpb-whatrequires --verbose -r 9 --distrib centos-stream --output ${LOG_FILE} redhat-rpm-config

  if [ $(cat ${LOG_FILE} | wc -l) -eq 0 ]
  then
    FAIL
  fi

  echo -e "${Blue}Look for rpm in reverse dependencies (c9s)${Color_Off}"

  if ! grep -qE '^rpm' ${LOG_FILE}
  then
    FAIL
  fi

  mv ${LOG_FILE}{,.${t_count}.dnf${i}}
  t_count=$((t_count + 1))
done

echo -e "${Blue}Compare DNF4 and DNF5 outputs${Color_Off}"

failed=0

for i in {1..${t_count}}
do
  diff -q ${LOG_FILE}.1.dnf4 ${LOG_FILE}.1.dnf5 > ${LOG_FILE}

  if [ $(cat ${LOG_FILE} | wc -l) -ne 0 ]
  then
    echo -e "${BRed}Test ${i} failed${Color_Off}"
    failed=1
    cat ${LOG_FILE}
  fi

  rm -f ${LOG_FILE}.${i}.dnf{4,5}
done

if [ ${failed} -ne 0 ]
then
  FAIL
fi

rm -f ${LOG_FILE}

echo -e "${BGreen}OK${Color_Off}"

echo ""
