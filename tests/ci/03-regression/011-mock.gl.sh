echo -e "${BBlue}##### Checking Mock backend #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

rm -rf ${EXCEPTION_LOG}

echo -e "${Blue}Mock back-end (dist-git)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config:
    committish: '@last_build'
revdeps:
  list:
    autoconf-archive:
      deps_only: True
name: mock-distgit
data: /tmp/.mpb/data
backend: mock
verbose: 5
EOF

PID=0
res=""

for i in {0..2}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 15
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Executing stage 3" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    destroyed=$(grep "Destroyed MpbDb instance" ${LOG_FILE} || true)
    if [ ! "x${destroyed}" == "x" ]
    then
      # We are done with this build
      break
    fi
    echo -en "${Purple}	Loop ${j} done${Color_Off}\r"

    sleep 20
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 5

  disown ${PID}
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi

  echo -e "${BPurple}Trial ${i}/2 failed${Color_Off}"
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  find "${HOME}/.mpb/mock_build/${ID}" -name mock.log -exec cat {} \;
  FAIL
fi

echo -e "${Blue}Mock back-end (git)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config:
    src_type: git
    src: https://src.fedoraproject.org/rpms/redhat-rpm-config.git
    committish: '@last_build'
revdeps:
  list:
    autoconf-archive:
      deps_only: True
name: mock-git
data: /tmp/.mpb/data
backend: mock
verbose: 5
EOF

PID=0
res=""

for i in {0..2}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 15
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Executing stage 3" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    destroyed=$(grep "Destroyed MpbDb instance" ${LOG_FILE} || true)
    if [ ! "x${destroyed}" == "x" ]
    then
      # We are done with this build
      break
    fi
    echo -en "${Purple}	Loop ${j} done${Color_Off}\r"

    sleep 20
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 5

  disown ${PID}
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi

  echo -e "${BPurple}Trial ${i}/2 failed${Color_Off}"
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  find "${HOME}/.mpb/mock_build/${ID}" -name mock.log -exec cat {} \;
  FAIL
fi

echo -e "${Blue}Mock back-end (file)${Color_Off}"
rm -f redhat-rpm-config*.rpm || true
dnf download --source redhat-rpm-config > /dev/null 2>&1 || true
dnf download --srpm redhat-rpm-config > /dev/null 2>&1 || true
fname=$(ls redhat-rpm-config*.src.rpm || true)
fpath=$(realpath ${fname})
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config:
    src_type: file
    src: ${fpath}
revdeps:
  list:
    autoconf-archive:
      deps_only: True
name: mock-file
data: /tmp/.mpb/data
backend: mock
verbose: 5
EOF

PID=0
res=""

for i in {0..0}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 15
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..6}
  do
    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Executing stage 3" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    destroyed=$(grep "Destroyed MpbDb instance" ${LOG_FILE} || true)
    if [ ! "x${destroyed}" == "x" ]
    then
      # We are done with this build
      break
    fi
    echo -en "${Purple}	Loop ${j} done${Color_Off}\r"

    sleep 20
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 5

  disown ${PID}
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi

  echo -e "${BPurple}Trial ${i}/2 failed${Color_Off}"
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  find "${HOME}/.mpb/mock_build/${ID}" -name mock.log -exec cat {} \;
  FAIL
fi

echo -e "${Blue}Cancel existing Mock${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
cancel: True
EOF

# Now we assume that any of the following MPB command may fail,
# but we want to see the logs if that happens.
mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
res=$(grep "Canceling ${ID}" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Collect existing Mock${Color_Off}"
mpb --collect -b ${ID} --collect-list='success' > ${LOG_FILE} || true

res=$(grep "Collecting data for mock" ${LOG_FILE} || true)
res1=$(tree -d -L 4 /tmp/.mpb/data | grep SUCCESS || true)

if [ "x${res}" == "x" ] || [ "x${res1}" == "x" ]
then
  tree -d -L 4 /tmp/.mpb/data
  FAIL
fi

echo -e "${Blue}Clean existing Mock${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
clean: True
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} || FAIL
res=$(grep "Cleaning build ${ID}" ${LOG_FILE} || true)
except=$(grep "Exception log stored" ${LOG_FILE} || true)

if [ "x${res}" == "x" ] && [ ! "x${except}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Mock back-end (stop/restart)${Color_Off}"
mock -r fedora-rawhide-x86_64 --scrub=all
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config:
    committish: '@last_build'
revdeps:
  list:
    autoconf-archive:
      committish: '@last_build'
      deps_only: True
name: mock-stop_start
data: /tmp/.mpb/data
backend: mock
verbose: 5
EOF

PID=0
res=""

for i in {0..2}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 10
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  # Kill first instance and create a new one
  kill -s INT ${PID} > /dev/null 2>&1 || true
  sleep 5
  disown ${PID}
  kill -s KILL ${PID} > /dev/null 2>&1 || true
  sleep 1

  except=$(grep "Exception log stored" ${LOG_FILE} || true)
  if [ ! "x${except}" == "x" ]
  then
    FAIL
  fi

  mpb -VVVVV -b ${ID} > ${LOG_FILE} 2>&1 &
  PID=$!
  sleep 5

  for j in {0..20}
  do
    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep -E "New status for .* is SUCCESS" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    destroyed=$(grep "Destroyed MpbDb instance" ${LOG_FILE} || true)
    if [ ! "x${destroyed}" == "x" ]
    then
      # We are done with this build
      break
    fi
    echo -en "${Purple}	Loop ${j} done${Color_Off}\r"

    sleep 20
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 5

  disown ${PID}
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi

  echo -e "${BPurple}Trial ${i}/2 failed${Color_Off}"
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  find "${HOME}/.mpb/mock_build/${ID}" -name mock.log -exec cat {} \;
  FAIL
fi

echo -e "${Blue}Mock back-end (stop/clean)${Color_Off}"
mock -r fedora-rawhide-x86_64 --scrub=all
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config:
    committish: '@last_build'
revdeps:
  list:
    autoconf-archive:
      committish: '@last_build'
name: mock-stop_clean
data: /tmp/.mpb/data
backend: mock
verbose: 5
EOF

PID=0
res=""

# In principle the command may fail if the package we are testing is broken
# Assume a real failure would imply an exception before the end of the process
mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
PID=$!
trial=0

ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)
while [ "x${ID}" == "x" ]
do
  sleep 1
  trial=$((trial + 1))
  if [ ${trial} -gt 40 ]
  then
    echo "No build ID found"
    FAIL
  fi
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)
done

trial=0
mock_build_before=$(ps -aux | grep "mock_build/${ID}/" | head -n -1 || true)
while [ "x${mock_build_before}" == "x" ]
do
  sleep 1
  trial=$((trial + 1))
  if [ ${trial} -gt 40 ]
  then
    echo "Mock never started: ${mock_build_before}"
    FAIL
  fi
  mock_build_before=$(ps -aux | grep "mock_build/${ID}/" | head -n -1 || true)
done

# Kill first instance and create a new one
kill -s INT ${PID} > /dev/null 2>&1 || true
sleep 2
disown ${PID}
kill -s KILL ${PID} > /dev/null 2>&1 || true
sleep 1

if [ "x${mock_build_before}" == "x" ]
then
  echo "After kill: ${mock_build_before}"
  FAIL
fi

# Clean should stop ongoing builds
mpb -VVVVV --clean -b ${ID} > ${LOG_FILE} 2>&1 &
PID=$!
sleep 5

mock_build_after=$(ps -aux | grep "mock_build/${ID}/" | head -n -1 || true)

if [ ! "x${mock_build_after}" == "x" ]
then
  echo "After clean: ${mock_build_before}"
  echo "After clean: ${mock_build_after}"
  FAIL
fi

res=$(grep "Cleaning build ${ID}" ${LOG_FILE} || true)
except=$(grep "Exception log stored" ${LOG_FILE} || true)

if [ "x${res}" == "x" ] && [ ! "x${except}" == "x" ]
then
  FAIL
fi

if [ -f ${EXCEPTION_LOG} ]
then
  except=$(grep "exception" ${EXCEPTION_LOG} || true)
  if [ ! "x${except}" == "x" ]
  then
    echo -e "${BRed}Exception raised during tests${Color_Off}"
    FAIL
  fi
fi

rm -f ${LOG_FILE} || true
rm -f ${EXCEPTION_LOG} || true

echo -e "${BGreen}OK${Color_Off}"

echo ""
