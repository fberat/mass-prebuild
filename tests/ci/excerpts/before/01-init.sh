if [ ! $(id -u) -eq 0 ]
then
  echo "Execute command with sudo"
  SUDO=sudo
fi

${SUDO} dnf install -y \
  npm \
  python3-dnf \
  python3-daemon \
  python3-koji \
  python3-pip \
  python3-filelock \
  python3-setuptools_scm \
  python3-argcomplete \
  git \
  ruby \
  patch \
  fedpkg \

if [ ! $(id -u) -eq 0 ]
then
  ${SUDO} usermod -a -G mock ${USER}
fi

python3 --version

pip install virtualenv

virtualenv venv --system-site-packages

source venv/bin/activate
