""" Mass pre-builder main interface

    The mass pre-builder is a set of tools meant to help the user to build its
    package and any reverse dependencies from it. The overall goal is to assess
    if a change in the main package obviously affects negatively its reverse
    dependencies. The infrastructure will not only build the packages but also
    execute the associated tests. If any error is found during the process, the
    user is informed from this failure.
"""

import argparse
from pathlib import Path
import os
import re
import sys
import threading

import argcomplete
import daemon
import yaml

from . import VERSION

from .backend import copr, mock, dummy, remote_mock, koji
from .backend.db import authorized_archs, authorized_collect, authorized_status_base, MpbDb
from .backend.package import MAIN_PKG, NO_BUILD, ALL_DEP_PKG
from .backend.log import MpbLog

def handle_thread_exception(args):
    """Redirect thread exception to the general exceptions"""
    if args.exc_type == SystemExit:
        return
    sys.excepthook(args.exc_type, args.exc_value, args.exc_traceback)


def handle_exception(exc_type, exc_value, exc_traceback):
    """Log exception into a file to ease debugging"""
    if exc_type == SystemExit:
        return

    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger().error('Stopping due to Uncaught exception !')
    old_level = logger().level
    logger().level = -2
    logger().logger.critical("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
    logger().level = max(min(old_level, 5), -2)
    logger().error(f'Exception log stored in {str(Path(Path.home() / ".mpb" / "mpb.log"))}')

    if current_backend() is not None:
        current_backend().stop_agents(False)

    release_database()


def database(config=None):
    """Setup and return the database"""
    if not hasattr(database, "database"):
        database.database = None

    if config:
        database.database = MpbDb(path=config['database'], verbose=config['verbose'])
        config['database'] = str(Path(database.database.path).resolve())

    return database.database


def logger():
    """Setup and return the logger"""
    if not hasattr(logger, "logger"):
        logger.logger = MpbLog()

    return logger.logger


def backends():
    """List of available back-ends"""
    return {
            'dummy': dummy.DummyBackend,
            'copr': copr.MpbCoprBackend,
            'mock': mock.MockBackend,
            'remote_mock': remote_mock.RemoteMockBackend,
            'koji': koji.MpbKojiBackend,
            }


def current_backend(config=None):
    """Global backend accessors"""
    if not hasattr(current_backend, "backend"):
        current_backend.backend = None

    if config:
        current_backend.backend = backends()[config['backend']](database(), logger(), config)

    return current_backend.backend


def print_config(config):
    """Print the current configuration if needed"""
    if all([not config['info'], not config['list_packages']]):
        logger().debug('Using the following config: ')
        logger().debug(f'{yaml.dump(config, default_flow_style=False)}')

def clean_builds(config):
    """Remove cleaned builds from the database"""
    if not config['clean_db']:
        return

    builds = database().builds()

    builds = [b for b in builds if b['state'] == 'FREE']

    for build in builds:
        database().remove(build['build_id'])


def list_builds(config):
    """Print a comprehensive list of available builds"""
    if not config['list']:
        return

    verbose = config['verbose']

    builds = database().builds()
    max_len = 14
    adjust = 0
    if verbose >= 2:
        adjust = 4

    for build in builds:
        if all([verbose < 1, build['name'].endswith('.checker')]):
            continue
        if all([verbose < 2, build['state'] == 'FREE']):
            continue
        max_len = max(max_len, len(build['name']) + adjust)

    build_id = 'Build ID'.center(16)
    name = 'Name'.center(max_len)
    backend = 'Backend'.center(16)
    state = 'Last state'.center(16)
    stage = 'Last stage'.center(16)

    print(f'+{16 * "-"}+-{max_len * "-"}-+{16 * "-"}+{16 * "-"}+{16 * "-"}+')
    print(f'|{build_id}| {name} |{backend}|{state}|{stage}|')
    print(f'+{16 * "-"}+-{max_len * "-"}-+{16 * "-"}+{16 * "-"}+{16 * "-"}+')

    for build in builds:
        if all([verbose < 1, build['name'].endswith('.checker')]):
            continue
        if all([verbose < 2, build['state'] == 'FREE']):
            continue

        build_id = str(build['build_id']).center(16)
        name = build['name']
        if build['state'] == 'FREE':
            name += ' (*)'
        name = name.ljust(max_len)
        config = yaml.safe_load(build['config'])
        backend = 'unknown'.center(16)

        if 'backend' in config:
            backend = config['backend'].center(16)

        state = build['state'].center(16)
        stage = str(build['stage']).center(16)

        print(f'|{build_id}| {name} |{backend}|{state}|{stage}|')

    print(f'+{16 * "-"}+-{max_len * "-"}-+{16 * "-"}+{16 * "-"}+{16 * "-"}+')

    if verbose >= 2:
        print('Build marked with (*) were removed from the remote back-end')

def list_packages(config):
    """List packages if requested"""
    if config['list_packages']:
        logger().debug(f'Package list for {config["build_id"]} ({current_backend().name})')
        logger().level = min(config['verbose'] + 1, 5)
        current_backend().print_packages()


def collect(config):
    """Collect data from build if requested"""
    if config['collect']:
        print(f'Collecting data for {current_backend().name}')
        current_backend().collect_data()


def cancel(config):
    """Cancel packages build if requested"""
    if config['cancel']:
        print(f'Canceling {config["build_id"]} ({current_backend().name})')
        current_backend().cancel()


def clean(config):
    """Clean build if requested"""
    if config['clean']:
        print(f'Cleaning build {config["build_id"]} ({current_backend().name})')
        current_backend().clean()


def infos(config):
    """Print build info if needed"""
    if config['info']:
        logger().info(f'Printing info about {config["build_id"]} ({current_backend().name})')
        logger().level = min(config['verbose'] + 1, 5)
        current_backend().info()

def rebuild(config):
    """Rebuild packages if required"""

    if not config['rebuild']:
        return

    print('Preparing to rebuild packages.')

    if isinstance(config['rebuild'], str):
        config['rebuild'] = config['rebuild'].split()

    if config['rebuild'][0].lower() == 'failed':
        status = [ 'FAILED', 'UNCONFIRMED', 'CHECK_NEEDED' ]
    else:
        status = authorized_status_base | { 'PENDING' }

    for pkg in current_backend().packages:
        if pkg.pkg_type & (MAIN_PKG | NO_BUILD):
            continue

        if not any([pkg.name in config['rebuild'],
                pkg.src_pkg_name in config['rebuild'],
                config['rebuild'][0].lower() in ['failed', 'all']]):
            continue

        # Ensure status is up to date
        pkg.check_status(pkg.pkg_type & ALL_DEP_PKG)
        if pkg.build_status not in status:
            continue

        current_backend().rebuild(pkg)

    old_conf = current_backend().config
    old_conf['stage'] = 3

    print('Reloading back-end to initiate rebuild.')
    current_backend(old_conf)

def _get_config(config_file):
    """Load a config file provided by the user"""
    try:
        with open(config_file, encoding='utf-8') as file:
            conf = {}
            try:
                conf = dict(yaml.safe_load(file))
            except yaml.YAMLError as exc:
                logger().error(exc)
            return conf
    except FileNotFoundError:
        if config_file is not None:
            logger().error(f'Can\'t open config file {config_file}')

    return {}


def get_config(config_file):
    """Load a config file provided by the user"""
    path = Path(config_file).resolve()
    if path.is_file():
        logger().debug(f'Loading {str(path)}')
        os.chdir(path.parent)
        return _get_config(str(path))

    path = Path("mpb.config").resolve()
    if path.is_file():
        logger().debug(f'Loading {str(path)}')
        return _get_config(str(path))

    home_path = Path(Path.home() / '.mpb')
    home_path.mkdir(parents=True, exist_ok=True)

    path = Path(home_path / 'config').resolve()
    if path.is_file():
        logger().debug(f'Loading {str(path)}')
        os.chdir(path.parent)
        return _get_config(str(path))

    return {}


def is_package_config_valid(config, ptype):
    """Make basic check on the configuration file"""
    ret = True

    if isinstance(config[ptype], str):
        config[ptype] = config[ptype].split()

    if isinstance(config[ptype], list):
        config[ptype] = {p: {'src_type': 'distgit'} for p in config[ptype]}

    if not isinstance(config[ptype], dict):
        logger().error(
            f'Provided config file is malformed (packages is no dictionary: {type(config[ptype])})'
        )
        return False

    for name, pkg in config[ptype].items():
        if not isinstance(pkg, dict):
            # Early adopters need to update their config[ptype] files
            logger().error(f'Config for {name} is malformed')
            ret = False
            continue

        if any(
            [
                not re.match(r'^@?[a-zA-Z0-9-._+]+', name),
                # Reject (s)rpm file names
                re.search(r'\.s?rpm$', name),
                re.search(r'\.src\.rpm$', name),
                # Reject NVR names
                re.search(r'\.[a-z]{1,3}[0-9]{1,4}$', name),
            ]
        ):
            logger().error(f'Package name "{name}" is invalid')
            logger().error('Don\'t provide source rpm, rpm or NEVRA names')
            ret = False

        pkg.setdefault('src_type', 'distgit')
        pkg.setdefault('skip_archs', '')
        pkg.setdefault('deps_only', False)

        src_type = pkg['src_type']
        if src_type in ['file', 'script']:
            if 'src' not in pkg:
                logger().error(f'Config is missing source for {name}:{src_type}')
                ret = False
                break

            path = Path(pkg['src']).resolve()
            if not path.is_file():
                logger().error(
                    f'Path provided for {pkg}:{src_type} doesn\'t exists (got {str(path)})'
                )
                ret = False
                break
            pkg['src']=str(path)

        if 'retry' in pkg:
            if pkg['retry'] == 'dynamic':
                pkg['retry'] = -1

            try:
                pkg['retry'] = int(pkg['retry'])
            except ValueError:
                logger().error(f'Retry count for {name} in invalid: {pkg["retry"]}')
                ret = False

        validate_skip_archs(pkg)

    return ret


def validate_packages(config):
    """Validate packages config"""
    # Check all elements separately so that user can see all errors at once
    logger().info('Checking for package configuration validity.')

    if 'packages' in config:
        config['validity'] = is_package_config_valid(config, 'packages')
    else:
        config['packages'] = {}

    if 'revdeps' not in config:
        config['revdeps'] = {}

    for ptype in ['list', 'append']:
        if not config['validity']:
            break

        if ptype not in config['revdeps']:
            continue

        config['validity'] = is_package_config_valid(config['revdeps'], ptype)

    config['revdeps'].setdefault('sample', 0)
    if not isinstance(config['revdeps']['sample'], int):
        logger().error(f"sample: invalid value {config['revdeps']['sample']}")
        config['validity'] = False

    if all([not config['packages'], config['build_id'] <= 0]):
        logger().error('No package list nor build ID given')
        config['validity'] = False


def validate_skip_archs(config):
    """Check if given archs are valid"""
    if 'skip_arch' in config:
        config['skip_archs'] = config['skip_arch']
        del config['skip_arch']

    if isinstance(config['skip_archs'], str):
        config['skip_archs'] = config['skip_archs'].split()

    if isinstance(config['skip_archs'], list):
        config['skip_archs'] = set(config['skip_archs'])

    if isinstance(config['skip_archs'], dict):
        config['skip_archs'] = set(config['skip_archs'].keys())

    for skip_arch in config['skip_archs']:
        if skip_arch not in authorized_archs:
            logger().error(f'{skip_arch} is not a supported skip_architecture')
            config['validity'] = False

    if 'all' in config['skip_archs']:
        logger().error('Skipping all architectures')
        config['validity'] = False


def validate_archs(config):
    """Check if given archs are valid"""
    if 'arch' in config:
        config['archs'] = config['arch']
        del config['arch']

    if isinstance(config['archs'], str):
        config['archs'] = config['archs'].split()

    if isinstance(config['archs'], list):
        config['archs'] = set(config['archs'])

    if isinstance(config['archs'], dict):
        config['archs'] = set(config['archs'].keys())

    for arch in config['archs']:
        if arch not in authorized_archs:
            logger().error(f'{arch} is not a supported architecture')
            config['validity'] = False


def validate_collect(config):
    """Validate collect config"""
    authorized_collect_ctrl = {
        f'control-{col}'.upper() for col in authorized_collect
    }
    accepted = sorted(authorized_collect) + sorted(authorized_collect_ctrl)
    accepted = [p.lower() for p in accepted]

    if isinstance(config['collect_list'], str):
        if ',' in config['collect_list']:
            config['collect_list'] = [
                col.strip() for col in config['collect_list'].split(',')
            ]
        else:
            config['collect_list'] = config['collect_list'].split()

    for col in config['collect_list']:
        if col.lower() not in accepted:
            logger().error(f'Invalid collect option "{col}"')
            logger().error(f'Accepted values: {accepted}')
            config['validity'] = False

    collect_set = set(config['collect_list'])

    for ctrl in ['', 'control-']:
        subcheck = {f'{ctrl}{col.lower()}' for col in authorized_collect}
        subset = collect_set & subcheck
        if all([len(subset - {f'{ctrl}log'}) == 0, f'{ctrl}log' in subset]):
            collect_set |= {f'{ctrl}failed'}

    for ctrl in ['', 'control-']:
        if f'{ctrl}failed' in collect_set:
            collect_set |= {f'{ctrl}check_needed', f'{ctrl}unconfirmed'}

    config['collect_list'] = list(collect_set)

    if all([config['collect'], config['build_id'] <= 0]):
        logger().error('Build ID missing for collect operation')
        config['validity'] = False


def release_database():
    """Release the database locks"""
    if not hasattr(release_database, 'lock'):
        release_database.lock = threading.Lock()

    if not hasattr(release_database, 'released'):
        release_database.released = False

    with release_database.lock:
        if all([database() is not None, not release_database.released]):
            database().release()
            release_database.released = True


def validate_database(config):
    """Validate the database"""
    if not database(config):
        config['validity'] = False


def validate_build_id(config):
    """Validate build ID"""
    if not config['build_id']:
        return

    if not database():
        logger().error('No valid database to check for build ID')
        config['build_id'] = 0
        return

    build = database().build_by_id(config['build_id'])

    if not build:
        logger().error(f'Build ID doesn\'t match any known ID {config["build_id"]}')
        config['build_id'] = 0


def validate_backend(user_config):
    """Validate the provided backend"""
    if any([not database(), user_config['build_id'] <= 0]):
        return

    # First try to get the backend from the database
    build = database().build_by_id(user_config['build_id'])
    build_config = yaml.safe_load(build['config'])

    backend = 'unknown'

    if 'backend' in build_config:
        backend = build_config['backend']

    if backend == 'unknown':
        backend = user_config['backend']
    else:
        user_config['backend'] = backend

    if backend not in backends():
        logger().error(f'Unknown back-end {backend}')
        user_config['validity'] = False


def validate_build_cmds(config):
    """Validate on shots commands"""
    validate_database(config)
    validate_build_id(config)
    validate_backend(config)

    if all([config['cancel'], config['build_id'] <= 0]):
        logger().error('Build ID missing for cancel operation')
        config['validity'] = False
        return
    if all([config['clean'], config['build_id'] <= 0]):
        logger().error('Build ID missing for clean operation')
        config['validity'] = False
        return
    if all([config['info'], config['build_id'] <= 0]):
        logger().error('Build ID missing for info operation')
        config['validity'] = False
        return
    if all([config['list_packages'], config['build_id'] <= 0]):
        logger().error('Build ID missing to list packages')
        config['validity'] = False
        return
    if all([config['rebuild'], config['build_id'] <= 0]):
        logger().error('Build ID missing for rebuild operation')
        config['validity'] = False
        return


def validate_config(config, is_checker=False):
    """Validate configuration inputs"""
    if not is_checker:
        validate_build_cmds(config)

    if any(
            [
                config['clean'],
                config['cancel'],
                config['list'],
                config['info'],
                config['list_packages'],
                not config['validity'],
                config['clean_db'],
            ]
        ):
        return

    if not is_checker:
        validate_packages(config)
    if config['retry'] == 'dynamic':
        config['retry'] = -1

    try:
        config['retry'] = int(config['retry'])
    except ValueError:
        logger().error(f'Retry count in invalid: {config["retry"]}')
        config['validity'] = False

    if all([config['stage'] >= 0, config['build_id'] <= 0]):
        logger().error('Stage can\'t be given without a valid build ID.')
        config['validity'] = False

    if config['backend'] not in backends():
        config['validity'] = False
        logger().error(f'Unknown back-end: {config["backend"]}')

    validate_archs(config)
    validate_skip_archs(config)

    # Ensure we have a boolean and not a random string
    config['enable_priorities'] = bool(config['enable_priorities'])

    if config['dnf_conf']:
        if not Path(config['dnf_conf']).is_file():
            logger().error(f'Can\'t find DNF config: {config["dnf_conf"]}')
            config['validity'] = False
        else:
            config['dnf_conf'] = str(Path(config['dnf_conf']).resolve())

    validate_collect(config)


class MpbAction(argparse.Action):
    """Mass pre-build action for argument parser

        This class implements an Action for the argument parser so that some of the arguments may
        take a build ID as an optional value for convenience.
    """
    def __call__(self, parser, namespace, value, option_string=None):
        if value in [ True, False ]:
            setattr(namespace, self.dest, value)
        else:
            setattr(namespace, 'buildid', int(value))
            setattr(namespace, self.dest, True)


def parse_args():
    """Setup argument parser"""
    authorized_collect_ctrl = {
        f'control-{col}'.upper() for col in authorized_collect
    }
    accepted = sorted(authorized_collect) + sorted(authorized_collect_ctrl)
    accepted = [p.lower() for p in accepted]

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--verbose',
        '-V',
        action='count',
        help='increase output verbosity, may be provided multiple times',
    )
    parser.add_argument(
        '--version',
        '-v',
        action='version',
        version=f'%(prog)s {VERSION}',
        help='show the version number and exit',
    )
    parser.add_argument(
        '--config',
        '-c',
        default='',
        help='''provide user specific config
                values in command line supersedes values in config file''',
    )
    parser.add_argument(
        '--stage',
        '-s',
        type=int,
        help='stage to directly begin with (needs a valid build ID)',
    )
    parser.add_argument('--buildid', '-b', type=int, help='build ID to work on')
    parser.add_argument(
        '--cancel',
        nargs='?',
        const=True,
        default=False,
        action=MpbAction,
        metavar='BUILDID',
        help='cancel any running package build from the given mass rebuild',
    )
    parser.add_argument(
        '--clean',
        nargs='?',
        const=True,
        default=False,
        action=MpbAction,
        metavar='BUILDID',
        help='''cancel build from the remote infrastructure (e.g. COPR) and delete the corresponding
                data from it. After this operation, there is still some information stored on the
                local host's database. This local information is retained until the '--clean-db'
                command is invoked.
        ''',
    )
    parser.add_argument(
        '--backend', help='choose a different backend from copr default'
    )

    parser.add_argument(
        '--collect',
        nargs='?',
        const=True,
        default=False,
        action=MpbAction,
        metavar='BUILDID',
        help='collect data for the given build id',
    )

    parser.add_argument(
        '--collect-list',
        nargs='?',
        const='',
        help=f'which data to collect, when collect stage is executed (accepted:{accepted})',
    )

    parser.add_argument(
        '--list',
        action='store_true',
        help='''list builds started locally.
                By default, it provides the active builds. Higher verbosity (through '-V') will show
                information about the 'checker' build. Even higher verbosity (through -VV) will also
                show builds marked as cleaned from the remote infrastructure, but for which there is
                still local information available.
        ''',
    )

    parser.add_argument(
        '--info',
        nargs='?',
        const=True,
        default=False,
        action=MpbAction,
        metavar='BUILDID',
        help='''Provide detailed information on a locally started build.
                This command is affected by the verbosity level ('-V'). The higher the verbosity is,
                the more detailed information you can get.
        ''',
    )

    parser.add_argument(
        '--list-packages',
        nargs='?',
        const=True,
        default=False,
        action=MpbAction,
        metavar='BUILDID',
        help='''list packages from a build.
                This command is affected by the verbosity level ('-V'). The higher the verbosity is,
                the more detailed information you can get.
        ''',
    )

    parser.add_argument(
        '--clean-db',
        action='store_true',
        help='''Definitely remove cleaned build from the local database.
                While '--clean' removes build information from the build infrastructure (e.g. COPR),
                the '--clean-db' removes build information still stored locally.
        ''',
    )

    parser.add_argument(
        '--rebuild',
        default=None,
        help='''Either a space separated list of packages to rebuild, or one of "failed", "all".
                Triggers the rebuild of the given packages, reset the stage to build stage.
                Only reverse dependencies are rebuilt, checker builds are left as-is.
                Package names are case sensitive.
                Build ID must be given through `-b BUILD_ID` for this command to be executed.
        '''
    )

    parser.add_argument(
            '--daemonize',
            '-d',
            action='store_true',
            help='''Daemonize MPB'''
    )

    argcomplete.autocomplete(parser)

    return parser.parse_args()


def populate_config():
    """Get config from file if given and initialize default values"""
    # pylint: disable = too-many-statements
    home_path = Path(Path.home() / '.mpb')
    home_path.mkdir(parents=True, exist_ok=True)

    args = parse_args()
    config = get_config(args.config)

    config['verbose'] = args.verbose or config.setdefault('verbose', 0)
    config['verbose'] = max(min(config['verbose'], 5), -2)
    logger().level = config['verbose'] # Verbosity is automatically converted

    config['backend'] = args.backend or config.setdefault('backend', 'copr')
    config['build_id'] = args.buildid or config.setdefault('build_id', 0)
    config.setdefault('base_build_id', 0)
    if args.stage is not None:
        config['stage'] = args.stage
    else:
        config.setdefault('stage', -1)
    config.setdefault('chroot', 'fedora-rawhide')
    config.setdefault('archs', 'x86_64')
    config.setdefault('skip_archs', '')
    config['cancel'] = args.cancel or config.setdefault('cancel', args.cancel)
    config['clean'] = args.clean or config.setdefault('clean', args.clean)
    config['clean_db'] = args.clean_db or config.setdefault('clean_db', args.clean_db)
    config.setdefault('data', str(Path(home_path / 'data')))
    config.setdefault('database', str(Path(home_path / 'share' / 'mpb.db')))
    config.setdefault('enable_priorities', False)
    config.setdefault('dnf_conf', '')
    config['collect'] = args.collect or config.setdefault('collect', args.collect)
    config['collect_list'] = args.collect_list or config.setdefault(
        'collect_list', 'failed control-log control-success control-failed'
    )
    config['list'] = args.list or config.setdefault('list', args.list)
    config['info'] = args.info or config.setdefault('info', args.info)
    config['list_packages'] = args.list_packages or config.setdefault('list_packages',
                                                                      args.list_packages)
    config.setdefault('retry', 0)
    config['daemonize'] = args.daemonize or config.setdefault('daemonize', False)
    config['validity'] = True

    if args.collect_list:
        if len(args.collect_list) > 0:
            config['collect_list_override'] = config.setdefault(
                'collect_list_override', True
            )
    else:
        config['collect_list_override'] = config.setdefault(
            'collect_list_override', False
        )
    config.setdefault('rebuild', args.rebuild)
    config.setdefault('checker', {})

    validate_config(config)

    # The checker build takes all the default from the main build, except the backend specific
    # configuration, which is reset by default.
    checker = {}
    if 'checker' in config:
        checker = config['checker'].copy()

    for key, value in config.items():
        config['checker'].setdefault(key, value)

    config['checker'].pop('db', None)
    config['checker'].pop('checker', None)
    # We don't want any packages in the checker by default
    config['checker'].pop('packages', None)
    config['checker'].pop('revdeps', None)

    for key in backends():
        # Let the backend handled their defaults
        config['checker'][key] = {}
        checker.setdefault(key, {})
        config['checker'][key].update(checker[key])

    validate_config(config['checker'], is_checker=True)

    return config


def main_loop():
    """Main mpb loop"""
    current_backend().start_agents()

    while not current_backend().is_completed():
        current_backend().execute_stage()
        current_backend().next_stage()

    current_backend().stop_agents()

def main():
    """Main function

    Setup argument parser, initialize default configuration, validates user
    input and execute the mass pre-build build.
    """
    # pylint: disable = too-many-statements
    logger()
    sys.excepthook = handle_exception
    threading.excepthook = handle_thread_exception

    config = populate_config()

    if any([config['validity'] is False, config['checker']['validity'] is False]):
        logger().error('Configuration is broken.')
        release_database()
        sys.exit(1)

    logger().debug(f'{__file__} v{VERSION}')
    logger().info(f'{Path(sys.argv[0]).name} v{VERSION}')

    logger().info(f'Using {config["backend"]} back-end')

    print_config(config)

    list_builds(config)
    clean_builds(config)

    if any([config['list'], config['clean_db']]):
        release_database()
        return

    data_path = Path(config['data']).resolve()
    data_path.mkdir(parents=True, exist_ok=True)
    config['data'] = str(data_path)

    try:
        current_backend(config)
    except ValueError as exc:
        logger().error(exc)
        release_database()
        sys.exit(1)

    # By default 'collect' is populated from the database
    # Override this value if the user asked for it
    if config['collect_list_override']:
        current_backend().collect = config['collect_list']

    infos(config)
    list_packages(config)
    collect(config)
    cancel(config)
    clean(config)
    rebuild(config)

    if any([config['info'],
            config['list_packages'],
            config['cancel'],
            config['clean'],
            config['collect'],
            ]):
        release_database()
        return

    daemon_context = None
    if config['daemonize']:
        print('Daemonizing')
        current_backend().init_checker_build()
        print(f'Main build ID: {current_backend().build_id}')
        print(f'Checker build ID: {current_backend().checker_build.build_id}')
        sys.stdout.flush()
        build_id = current_backend().build_id
        old_conf = current_backend().config
        release_database()

        daemon_context = daemon.DaemonContext()
        daemon_context.files_preserve = [ logger().file_handler.stream.fileno() ]
        daemon_context.open()

        old_conf['build_id'] = build_id
        database(old_conf)
        current_backend(old_conf)

    main_loop()

    release_database()

    if config['daemonize']:
        daemon_context.close()

if __name__ == '__main__':
    main()
