""" Koji back-end implementation

    This module implements the Koji specific functionalities for the mass
    pre-builder.
"""

from datetime import datetime, timedelta
from pathlib import Path
import subprocess
import threading
import yaml

import koji
from koji_cli.lib import activate_session

from .package import MpbPackage, MAIN_PKG, NO_BUILD
from .backend import MpbBackend

CONNECTION_TRIALS = 500
BATCH_LIMIT = 50

arch_table = {
    'aarch64': {'default': 'aarch64'},
    'i386': {'default': 'x86_64', 'fedora': 'i686'},
    'i586': {'default': 'x86_64', 'fedora': 'i686'},
    'i686': {'default': 'x86_64', 'fedora': 'i686'},
    'ppc64le': {'default': 'ppc64le'},
    's390x': {'default': 's390x'},
    'x86': {'default': 'x86_64', 'fedora': 'i686'},
    'x86_64': {'default': 'x86_64'},
}

# Deduced from koji_cli/lib.py
task_states = {
    'FREE': 0,
    'OPEN': 1,
    'CLOSED': 2,
    'CANCELED': 3,
    'ASSIGNED': 4,
    'FAILED': 5,
}


class MpbKojiPackage(MpbPackage):
    """Mass pre-build koji specific package implementation

    Note: Architecture restrictions are ignored for Koji backend. All arches supported by a package
    are always built.

    Attributes:
        pkg_id: The package identifier for cross referencing between packages
        name: The name of the package
        base_build_id:
            The identifier of the mass pre-build the package is built on
        build_id: Build ID on the infrastructure
        build_status: A dictionary of build status per supported arch
        pkg_type: The type of the package, between main and reverse dependency
        skip_archs: The set of non-supported arch (to ease filtering)
        src_type: The source type (distgit, url, file, ...)
        src: The actual source
        committish: The tag, branch, commit ID ... For distgits ant gits
        priority: The priority for build batches
        after_pkg: A package ID we need to wait for to be able to build
        with_pkg: A package ID we build together with

    """

    def _build_generic(self):
        """Wrapper around the infrastructure specific build method"""
        if self.pkg_type & MAIN_PKG:
            opts = {'scratch': False, 'draft': True}
        else:
            opts = {'scratch': True, 'draft': False}

        self.build_id = self._owner.session.build(self.src, self.name, opts, priority=15)
        self._update_package()

    def _build_distgit(self):
        """Infrastructure specific build method"""
        self.collect_build_info()
        if self.src.find('#') < 0:
            self.src = f'{self.src}#{self.committish}'

        self._build_generic()

    def _build_file(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_git(self):
        """Infrastructure specific build method"""
        if not self.src.startswith('git+'):
            self.src = f'git+{self.src}'

        if self.src.find('#') < 0:
            self.src = f'{self.src}#{self.committish}'

        self._build_generic()

    def _build_url(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_script(self):
        """Infrastructure doesn't support script builds"""
        raise NotImplementedError('Script builds not supported by this backend')

    def check_status(self, check_needed=True):
        """Infrastructure specific build method"""
        if not self.build_id:
            return True

        self._owner.monitor_project()

        task = None

        for build in self._owner.build_list:
            if build['id'] == self.build_id:
                task = build
                break

        if task is None:
            return False

        status = 'PENDING'

        if task['state'] in [task_states['FREE'], task_states['ASSIGNED']]:
            status = 'PENDING'

        if task['state'] == task_states['OPEN']:
            status = 'RUNNING'

        if task['state'] == task_states['CLOSED']:
            status = 'SUCCESS'

        if task['state'] in [task_states['FAILED'], task_states['CANCELED']]:
            if check_needed:
                status = 'UNCONFIRMED'
            else:
                status = 'FAILED'

        if self.build_status != status:
            self._logger.debug(f"New status for {self.name} is {status}")
            self._logger.debug(f"Koji data: {task}")

        self.build_status = status

        return True

    def location(self):
        """Get URL of the package"""
        meta = self._owner.whatrequires.metalinks()[self._owner.distgit]
        if self._owner.releasever in meta:
            if 'koji' in meta[self._owner.releasever]:
                meta = meta[self._owner.releasever]

        address = meta['koji']
        if 'koji' in self._owner.config:
            address = self._owner.config['koji'].get('address', meta['koji'])

        return f'{address.replace("kojihub","koji")}/taskinfo?taskID={self.build_id}'

    def cancel(self):
        """Infrastructure specific cancel method"""
        if not self.build_id:
            return

        self._owner.session.cancelTask(self.build_id)
        self.build_status = 'FAILED'

    def collect_data(self, dest, logs_only=False, failed_only=True):
        """Infrastructure specific data collector method"""
        self._logger.debug(f'Collecting data for {self.src_pkg_name}')

        base_dest = f'{dest}/{self._owner.name}/{self._owner.chroot}'
        task_list = self._owner.session.listTasks(opts={'parent': self.build_id})

        for task in task_list:
            if all([failed_only,
                    task['state'] not in [task_states['FAILED'],
                                          task_states['CANCELED']]]):
                self._logger.debug(f'Skipping {self.src_pkg_name} {task}')
                continue
            outputs = self._owner.session.listTaskOutput(task['id'])
            for output in outputs:
                destdir = Path(f'{base_dest}/{self.src_pkg_name}/{task["label"]}/')
                destdir.mkdir(parents=True, exist_ok=True)
                if all([logs_only, output.endswith('.rpm')]):
                    continue
                self._logger.debug(f'Collecting {output}')
                with open(f'{str(destdir)}/{output}', 'w+b') as file:
                    file.write(self._owner.session.downloadTaskOutput(task['id'], output))

    def clean(self):
        """Infrastructure specific clean method"""
        # Garbage collection is left to the infrastructure


class MpbKojiBackend(MpbBackend):
    """Mass pre-build Koji back-end

        Implement Koji specific functionalities.

    Attributes:
        client: The Koji client instance

    """

    def __init__(self, database, logger, config):
        """Call the super class, and initialize the Koji client"""
        super().__init__(database, logger, config)

        self.build_list = None
        self._monitoring = threading.Lock()
        self._next_check = 0
        self.reset_monitor()

        meta = self.whatrequires.metalinks()[self.distgit]
        if self.releasever in meta:
            if 'koji' in meta[self.releasever]:
                meta = meta[self.releasever]

        address = meta['koji']
        self._base_tag_suffix = 'build'
        if 'koji' in config:
            address = config['koji'].get('address', meta['koji'])
            self._base_tag_suffix = config['koji'].get('base_tag_suffix', 'build')

        self.session = koji.ClientSession(address)
        options = {
                'authtype': 'kerberos',
                'debug': False,
                'cert': '',
                'keytab': None,
                'principal': None,
                   }
        activate_session(self.session, options)
        self.prepared = False

    def _build_batch(self, pkg_list):
        """Start multiple builds in a batch"""
        # pylint: disable = not-callable
        if not pkg_list:
            return

        opts = {}
        if next(iter(pkg_list)).pkg_type & MAIN_PKG:
            opts = {'scratch': False, 'draft': True}
        else:
            opts = {'scratch': True, 'draft': False}

        # The target is the side tag we created in the prepare step
        # self.name is used to store side tag info
        with self.session.multicall() as mcall:
            task_list = [mcall.build(pkg.src, self.name, opts, priority=15) for pkg in pkg_list]

        for index, pkg in enumerate(pkg_list):
            self._logger.debug(f'{task_list[index].result}')
            pkg.build_id = task_list[index].result

            # By side effect, this will get the build ID and source saved into the database
            pkg.build_status = 'PENDING'

            self._build_list.remove(pkg)
            self._watch_list.append(pkg)

    def _build_packages(self, pkg_list):
        """ Batch builds for a list of packages

            Due to Koji load limitations, we don't queue more than BATCH_LIMIT builds at once.
            Thus, we wait before sending another batch as long as there are still non-running
            builds.
        """
        if self.monitor_project():
            return

        if not pkg_list:
            return

        batch = []
        for pkg in pkg_list[:BATCH_LIMIT]:
            pkg.build_id = 0
            pkg.build_status = 'FREE'
            if pkg.pkg_type & NO_BUILD:
                pkg.build_status = 'SUCCESS'
            else:
                if pkg.src_type in ['git', 'distgit']:
                    pkg.collect_build_info()
                if all([pkg.src_type == 'git', not pkg.src.startswith('git+')]):
                    pkg.src = f'git+{pkg.src}'

                if all([pkg.src_type in ['distgit', 'git'], pkg.src.find('#') < 0]):
                    pkg.src = f'{pkg.src}#{pkg.committish}'

                batch.append(pkg)

        self._build_batch(batch)

        self._watch_event.set()

    def _get_package_class(self):
        """Return the package class to be used internally"""
        return MpbKojiPackage

    def prepare(self):
        """Setup the Koji project and call super class method"""
        if self.prepared:
            return

        base_tag = self.committish

        # Retrieve the actual rawhide release name from bodhi
        if base_tag == 'rawhide':
            cmd = ['curl', 'https://bodhi.fedoraproject.org/releases/?state=pending']
            result = subprocess.run(cmd,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.DEVNULL,
                                    text=True,
                                    check=False)
            releases = yaml.safe_load(result.stdout)

            for release in releases['releases']:
                if release['branch'] == 'rawhide':
                    base_tag = release['dist_tag']

        if self._base_tag_suffix:
            base_tag = f'{base_tag}-{self._base_tag_suffix}'

        tag_info = None
        if self.name:
            tag_info = self.session.getTag(self.name)

        if tag_info is None:
            side_tag = self.session.createSideTag(base_tag)
            self.name = side_tag['name']
            self._logger.debug(f'Got {side_tag}')

        self.prepared = True
        super().prepare()

    def clean(self):
        """Execute super class method and destroy Koji project"""
        super().clean()

        try:
            self.session.removeSideTag(self.name)
        except koji.GenericError:
            self._logger.debug(f'No tag to remove ({self.name})')

    def location(self):
        """Get URL of the project"""
        meta = self.whatrequires.metalinks()[self.distgit]
        if self.releasever in meta:
            if 'koji' in meta[self.releasever]:
                meta = meta[self.releasever]

        address = meta['koji']
        if 'koji' in self.config:
            address = self.config['koji'].get('address', meta['koji'])

        # Note: tagID may actually be either the ID or the Name
        return f'{address.replace("kojihub","koji")}/taginfo?tagID={self.name}'

    def reset_monitor(self):
        """Force monitor to get the build list on next run"""
        with self._monitoring:
            self._next_check = datetime.now() - timedelta(days=1)

    def monitor_project(self):
        """Get the whole project's package data

            Returns: The amount of pending builds
        """
        # pylint: disable = not-callable
        with self._monitoring:
            if datetime.now() > self._next_check:
                res = []
                with self.session.multicall() as mcall:
                    res = [mcall.getTaskInfo(p.build_id) for p in self.packages if p.build_id]
                self.build_list = [r.result for r in res]
                self._next_check = datetime.now() + timedelta(seconds=10)
                self._logger.debug(f'Updated build list: {self.build_list}')

        return len([b for b in self.build_list if b['state'] == 'free'])
