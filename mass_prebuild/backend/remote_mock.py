""" Remote back-end implementation

    The remote back-end implementation makes use of the mock back-end.
    It uses ssh to execute all the mock commands.
"""

import shutil

from .executor import MpbExecutor
from .mock import MockBackend


class RemoteMockBackend(MockBackend):
    """ Remote back-end implementation

    The remote back-end implementation makes use of the mock back-end, and is
    intended for use on a remote system. It uses ssh to execute the mock back-end
    commands.

    Args:
        database (dict): A dictionary representing the project's database.
        logger (MpbLogger): An instance of the Logger class for logging
            purposes.
        config (dict): A dict object containing configuration settings for
            this backend.

    Attributes:
        _remote_address (str): The address of the remote system, usually in
            the form 'user@host'.
        executor (MpbExecutor): An instance of MpbExecutor class for running
            commands on the remote system.
    """

    def __init__(self, database, logger, config):
        """
        Initialize the RemoteMockBackend with a given project's database,
        logger, and configuration settings. Also initialize an MpbExecutor
        instance for handling of remote command execution.
        """
        tools = {
                'scp': {
                    'path': shutil.which("scp"),
                    'mandatory': True
                    },
                'ssh': {
                    'path': shutil.which("ssh"),
                    'mandatory': True
                    },
                }

        for name, tool in tools.items():
            if any([not tool['mandatory'], tool['path']]):
                continue
            raise RuntimeError(f'Unable to locate {name} to process build')

        remote = config['remote']['address']
        executor = MpbExecutor(logger, tools['ssh']['path'], tools['scp']['path'], remote)

        test_ssh_cmd = [tools['ssh']['path'],
                        remote,
                        '-q',
                        '-o', 'BatchMode=yes',
                        '-o', 'StrictHostKeyChecking=no',
                        '-o', 'ConnectTimeout=5',
                        'exit 0']

        executor.exec(test_ssh_cmd, local=True).check_returncode()

        super().__init__(database, logger, config, executor)

    def location(self):
        """Get URL of the project"""
        return f"{self.config['remote']['address']}:{self.build_path}"
