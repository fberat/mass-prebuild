""" Dummy back-end implementation, used for basic testing

    This module is used to test the general functionalities of the back-end
    implementation.
"""

import random
import time

from .package import MpbPackage
from .backend import MpbBackend


class DummyPackage(MpbPackage):
    """Mass pre-build dummy package implementation

    Attributes:
        pkg_id: The package identifier for cross referencing between packages
        name: The name of the package
        base_build_id:
            The identifier of the mass pre-build the package is built on
        build_id: Build ID on the infrastructure
        build_status: A dictionary of build status per supported arch
        pkg_type: The type of the package, between main and reverse dependency
        skip_archs: The set of non-supported arch (to ease filtering)
        src_type: The source type (distgit, url, file, ...)
        src: The actual source
        committish: The tag, branch, commit ID ... For distgits ant gits
        priority: The priority for build batches
        after_pkg: A package ID we need to wait for to be able to build
        with_pkg: A package ID we build together with

    """

    def _build_distgit(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_file(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_git(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_script(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_url(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_any(self):
        """Infrastructure specific build method"""
        self.build_id = int(time.time_ns())

        self._update_package()

    def _generate_build_status(self, check_needed=True):
        """Infrastructure specific build method"""
        val = random.randint(0, 2)
        if val != 0:
            return random.choice(['PENDING', 'RUNNING', 'RUNNING'])

        rand = random.randint(0, 5)
        if rand > 0:
            return 'SUCCESS'

        if check_needed:
            return 'CHECK_NEEDED'

        return 'FAILED'

    def check_status(self, check_needed=True):
        """Infrastructure specific build method"""
        if not self.build_id:
            return True

        if self.build_status in [ 'FAILED', 'SUCCESS', 'UNCONFIRMED' ]:
            return True

        status = self._generate_build_status(check_needed)

        if self.build_status != status:
            self._logger.debug(f"New status for {self.name} is {status}")

        self.build_status = status

        return True

    def cancel(self):
        """Infrastructure specific cancel method"""
        if self.build_status != 'SUCCESS':
            self.build_status = 'FAILED'

    def collect_data(self, dest, logs_only=False, failed_only=True):
        """Infrastructure specific data collector method"""
        if any([not failed_only, self.build_status in ['FAILED', 'UNCONFIRMED']]):
            self._logger.debug(f'Collecting data (logs only? {logs_only}) for {self.name}')

    def clean(self):
        """Infrastructure specific clean method"""
        self._logger.debug(f'Cleaning for {self.name}')


class DummyBackend(MpbBackend):
    """Dummy backend implementation"""

    def _get_package_class(self):
        """Return the package class to be used internally"""
        return DummyPackage
