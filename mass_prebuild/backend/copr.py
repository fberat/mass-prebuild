""" COPR back-end implementation

    This module implements the COPR specific functionalities for the mass
    pre-builder.
"""

from datetime import datetime, timedelta
import gzip
from io import BytesIO
from pathlib import Path
import random
import subprocess
import tempfile
import time
import threading

import pycurl
import yaml

from copr.v3 import Client, exceptions, config_from_file

from .package import MpbPackage, ALL_DEP_PKG
from .backend import MpbBackend

CONNECTION_TRIALS = 500

BACKEND_PKG_FAIL = 0
BACKEND_EXCLUDE_ARCH = 1 << 0
BACKEND_REBUILD = 1 << 1

arch_table = {
    'aarch64': {'default': 'aarch64'},
    'i386': {'default': 'x86_64', 'fedora': 'i386'},
    'i586': {'default': 'x86_64', 'fedora': 'i386'},
    'i686': {'default': 'x86_64', 'fedora': 'i386'},
    'ppc64le': {'default': 'ppc64le'},
    's390x': {'default': 's390x'},
    'x86': {'default': 'x86_64', 'fedora': 'i386'},
    'x86_64': {'default': 'x86_64'},
}

EXCLUDE_ARCH = ['Architecture is excluded']
COPR_FAILED = [
               'Cannot download, all mirrors were already tried without success',
               'Errors during downloading metadata for repository',
              ]
ALL_EXCLUSIONS = EXCLUDE_ARCH + COPR_FAILED


def check_logs(base_url):
    """Look for exclusion patterns in a build log"""
    all_exclusions = []
    filename = 'builder-live.log.gz'

    with tempfile.TemporaryDirectory() as tempdirname:
        cmd = ['wget', '-q', '-P', tempdirname, base_url + filename]
        subprocess.call(cmd)
        try:
            with gzip.open(f'{tempdirname}/{filename}', 'rt') as file:
                all_exclusions = [line for line in file if any(
                    x in line for x in ALL_EXCLUSIONS
                    )]
        except FileNotFoundError:
            return BACKEND_EXCLUDE_ARCH

    find = [line for line in all_exclusions if any(
        x in line for x in EXCLUDE_ARCH
        )]
    if find:
        return BACKEND_EXCLUDE_ARCH
    find = [line for line in all_exclusions if any(
        x in line for x in COPR_FAILED
        )]
    if find:
        return BACKEND_REBUILD

    return BACKEND_PKG_FAIL


def arch_to_chroot(base, arch, distrib):
    """Convert an arch to a chroot"""
    chroot = f'{base}-{arch}'

    if arch in arch_table:
        if distrib in arch_table[arch]:
            chroot = f'{base}-{arch_table[arch][distrib]}'
        else:
            chroot = f'{base}-{arch_table[arch]["default"]}'

    return chroot


def get_accepted_chroots(base, archs, distrib):
    """Compute accepted chroot list from a chroot base,
       the architecture list and a distribution name
    """
    accepted_chroots = []
    for arch in archs:
        accepted_chroots.append(arch_to_chroot(base, arch, distrib))

    return accepted_chroots


class MpbCoprPackage(MpbPackage):
    """Mass pre-build copr specific package implementation

    Attributes:
        pkg_id: The package identifier for cross referencing between packages
        name: The name of the package
        base_build_id:
            The identifier of the mass pre-build the package is built on
        build_id: Build ID on the infrastructure
        build_status: A dictionary of build status per supported arch
        pkg_type: The type of the package, between main and reverse dependency
        skip_archs: The set of non-supported arch (to ease filtering)
        src_type: The source type (distgit, url, file, ...)
        src: The actual source
        committish: The tag, branch, commit ID ... For distgits ant gits
        priority: The priority for build batches
        after_pkg: A package ID we need to wait for to be able to build
        with_pkg: A package ID we build together with

    """

    _build_data = None

    def _call_build_func(self, options):
        """Call the COPR build function corresponding to the source type"""
        # Note: new switch case feature from Python 3.10 can't be used as we
        # should support Python 3.9
        if self.src_type == 'git':
            return self._owner.client.build_proxy.create_from_scm(
                self._owner.ownername,
                self._owner.name,
                self.src,
                committish=self.committish,
                buildopts=options,
            )

        if self.src_type == 'file':
            return self._owner.client.build_proxy.create_from_file(
                self._owner.ownername,
                self._owner.name,
                self.src,
                buildopts=options,
            )

        if self.src_type == 'url':
            return self._owner.client.build_proxy.create_from_url(
                self._owner.ownername,
                self._owner.name,
                self.src,
                buildopts=options,
            )

        # Default: distgit
        return self._owner.client.build_proxy.create_from_distgit(
            self._owner.ownername,
            self._owner.name,
            self.src_pkg_name,
            committish=self.committish,
            distgit=self._owner.distgit,
            buildopts=options,
        )

    def _build_generic(self):
        """Wrapper around the infrastructure specific build method"""
        accepted_chroots = get_accepted_chroots(self._owner.chroot,
                                                self.archs,
                                                self._owner.distgit)

        background = all([
            any([
                self._owner.config['copr']['background'] == 'all',
                self.pkg_type & ALL_DEP_PKG,
               ]),
            self.src_pkg_name not in self._owner.config['copr']['disable_background'],
            self.name not in self._owner.config['copr']['disable_background'],
           ])

        opts = {
            'timeout': self._owner.config['copr']['timeout'],
            'chroots': accepted_chroots,
            'background': background,
            }

        # COPR interface forbids to provide both values simultaneously (even if
        # one as a 0 value). We therefore need to choose one.
        # The with_pkg as the highest priority, as after_pkg is always set if
        # priority is non-0 and with_pkg is reset whenever after_pkg is modified.
        # See backend->_populate_packages for more details.
        if self.with_pkg:
            opts['with_build_id'] = self.with_pkg
        else:
            if self.after_pkg:
                opts['after_build_id'] = self.after_pkg

        while True:
            try:
                pkg_build = self._owner.copr_wrapper(self._call_build_func, opts)
                break
            except exceptions.CoprRequestException as exc:
                # With/after build may not be running anymore, forget it.
                if exc.result['error'].startswith('Bad request parameters'):
                    if 'after_build_id' in opts:
                        opts['after_build_id'] = 0
                    if 'with_build_id' in opts:
                        opts['with_build_id'] = 0
                    continue

                raise

        self._owner.reset_monitor()
        self.build_id = pkg_build['id']
        self._update_package()

    def _build_distgit(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_file(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_git(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_url(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_script(self):
        """Infrastructure specific build method"""
        return

    def _check_backend_failure(self):
        """Check if a failed build isn't due to a failure in the backend"""
        build_chroots = self._owner.copr_wrapper(self._owner.client.build_chroot_proxy.get_list,
                                                 self.build_id)

        build_state = 'SUCCESS'
        for chroot in build_chroots:
            state = chroot['state'].upper()
            if state == 'CANCELED':
                return 'FAILED'

            if state != 'FAILED':
                continue

            arch = ''
            for _arch in self._owner.archs:
                if arch_to_chroot(self._owner.chroot, _arch, self._owner.distgit) == _arch:
                    arch = _arch
                    break

            if state == 'SKIPPED':
                state = 'SUCCESS'
                self.skip_archs |= {arch}
                continue

            if not chroot['result_url']:
                return 'FAILED'

            ret = check_logs(chroot['result_url'])
            if ret & BACKEND_EXCLUDE_ARCH:
                state = 'SUCCESS'
                self.skip_archs |= {arch}
                continue

            self.skip_archs -= {arch}

            if ret & BACKEND_REBUILD:
                state = 'PENDING'
                if self.backend_failures > 0:
                    build_state = state
                    self.backend_failures -= 1
                continue

            if state == 'FAILED':
                return 'FAILED'

        if build_state == 'PENDING':
            self._logger.debug(f'Rebuilding {self.name} due to backend failure')
            self._build_generic()

        return build_state

    def _set_build_status(self, check_needed=True):
        """Infrastructure specific build method"""
        if not self._build_data:
            return

        status = 'PENDING'

        if self._build_data['state'] in ['succeeded', 'forked', 'skipped']:
            status = 'SUCCESS'

        if self._build_data['state'] in ['starting', 'running']:
            status = 'RUNNING'

        if self._build_data['state'] in ['failed', 'canceled']:
            status = self._check_backend_failure()

        if all([status == 'FAILED', check_needed]):
            status = 'CHECK_NEEDED'

        if self.build_status != status:
            self._logger.debug(f'New status for {self.name} is {status}')

        self.build_status = status

    def check_status(self, check_needed=True):
        """Infrastructure specific build method"""
        # pylint: disable = too-many-branches
        if not self.build_id:
            return True

        if self.build_status in ['SUCCESS', 'FAILED', 'UNCONFIRMED']:
            return True

        if not self._owner.monitor_project():
            return False

        # In case the user as manually restarted a build for this package
        # Try to update information about the build ID
        for build in self._owner.build_list:
            if build['source_package']['name'] == self.src_pkg_name:
                if build['id'] > self.build_id:
                    self.build_id = build['id']

        # Get build data using the build ID
        for build in self._owner.build_list:
            if build['id'] == self.build_id:
                self._build_data = build
                break

        if not self._build_data:
            # Last resort, explicitly get latest build for this package
            pkg = self._owner.copr_wrapper(self._owner.client.package_proxy.get,
                                           self._owner.ownername,
                                           self._owner.name,
                                           self.src_pkg_name,
                                           with_latest_build=True,
                                           )
            if pkg:
                self._build_data = pkg['builds']['latest']

        if self._build_data:
            self.build_id = self._build_data['id']
        else:
            # The package exists, but there is no build: assume the user deleted it
            self.build_id = 0
            self.build_status = 'FAILED'

        self._set_build_status(check_needed)

        return True

    def cancel(self):
        """Infrastructure specific cancel method"""
        if not self.build_id:
            return

        try:
            self._owner.copr_wrapper(self._owner.client.build_proxy.cancel, self.build_id)
            self.build_status = 'FAILED'
        except exceptions.CoprRequestException as exc:
            if not exc.result['error'].startswith('Cannot cancel build'):
                raise

    def _prepare_multi_curl(self, num_conn):
        """Pre-allocate a list of curl objects"""
        m = pycurl.CurlMulti()
        m.handles = [None] * num_conn
        for i in range(0, num_conn):
            c = pycurl.Curl()
            c.fp = None
            c.setopt(pycurl.FOLLOWLOCATION, 1)
            c.setopt(pycurl.MAXREDIRS, 5)
            c.setopt(pycurl.CONNECTTIMEOUT, 30)
            c.setopt(pycurl.TIMEOUT, 300)
            c.setopt(pycurl.NOSIGNAL, 1)
            m.handles[i] = c

        self._logger.debug(f"Prepared {len(m.handles)} ({num_conn}) objects")
        return m

    def _cleanup_multi_curl(self, m):
        """Cleanup the multi curl handles"""
        # Cleanup
        for c in m.handles:
            if c.fp is not None:
                c.fp.close()
                c.fp = None
            c.close()
        m.close()

    def _collect_get_file_list(self, base_url, logs_only):
        """Retrieve the list of files to be collected"""
        buffer = BytesIO()
        c = pycurl.Curl()
        rsync = f'{base_url}build-{self.build_id:08}.rsync.log'
        # pylint: disable=no-member
        c.setopt(c.URL, rsync)
        c.setopt(c.WRITEDATA, buffer)
        c.perform()

        if c.getinfo(c.RESPONSE_CODE) != 200:
            return []

        c.close()
        # pylint: enable=no-member

        self._logger.debug(f'Getting rsync:\n{rsync}')
        self._logger.debug(buffer.getvalue().decode('utf-8'))
        queue = [f for f in buffer.getvalue().decode('utf-8').split('\n')
                 if all(["chroot_scan" not in f, ' ' not in f, './' not in f, f])]
        queue.append('builder-live.log.gz')

        if logs_only:
            queue = [f for f in queue if '.rpm' not in f]

        return queue

    def _collect_data(self, dest, base_url, logs_only=False):
        """Wrapper around curl, adapted from pycurl multi example"""
        # pylint: disable=too-many-locals
        queue = self._collect_get_file_list(base_url, logs_only)

        if not queue:
            return

        num_urls = len(queue)

        # This path is legacy, keeping it similar to old wget implementation
        path = Path(f"{dest}/{self.src_pkg_name}/{self.build_id:08}-{self.src_pkg_name}")
        path.mkdir(parents=True, exist_ok=True)

        m = self._prepare_multi_curl(len(queue))

        # Main loop
        freelist = m.handles[:]
        num_processed = 0
        while num_processed < num_urls:
            # If there is an url to process and a free curl object, add to multi stack
            while queue and freelist:
                filename = queue.pop(0)
                c = freelist.pop()
                # pylint: disable=consider-using-with
                c.fp = open(f'{str(path)}/{filename}', "wb")
                # pylint: enable=consider-using-with
                c.setopt(pycurl.URL, f"{base_url}{filename}")
                c.setopt(pycurl.WRITEDATA, c.fp)
                m.add_handle(c)
                # store some info
                c.filename = filename
                c.url = f"{base_url}/{filename}"
            # Run the internal curl state machine for the multi stack
            while 1:
                ret, num_handles = m.perform()
                if ret != pycurl.E_CALL_MULTI_PERFORM:
                    break
                self._logger.debug(f"{num_handles} remaining tasks")
            # Check for curl objects which have terminated, and add them to the freelist
            while 1:
                num_q, ok_list, err_list = m.info_read()
                for c in ok_list:
                    c.fp.close()
                    c.fp = None
                    m.remove_handle(c)
                    self._logger.info(
                            f"Success: {c.filename} {c.url} {c.getinfo(pycurl.EFFECTIVE_URL)}")
                    freelist.append(c)
                for c, errno, errmsg in err_list:
                    c.fp.close()
                    c.fp = None
                    m.remove_handle(c)
                    self._logger.warning(f"Failed: {c.filename} {c.url} {errno} {errmsg}")
                    freelist.append(c)
                num_processed = num_processed + len(ok_list) + len(err_list)
                if num_q == 0:
                    break
            # Currently no more I/O is pending, could do something in the meantime
            # (display a progress bar, etc.).
            # We just call select() to sleep until some more data is available.
            m.select(1.0)

        self._cleanup_multi_curl(m)

    def collect_data(self, dest, logs_only=False, failed_only=True):
        """Infrastructure specific data collector method"""
        self._logger.debug(f'Collecting data for {self.src_pkg_name}')

        build = self._owner.copr_wrapper(self._owner.client.build_proxy.get, self.build_id)

        if not build:
            self._logger.warning(f'Build not found for {self.name}')
            return

        chroots = self._owner.copr_wrapper(self._owner.client.build_chroot_proxy.get_list,
                                           self.build_id)

        for chroot in chroots:
            arch = ''
            for _arch in self._owner.archs:
                if arch_to_chroot(self._owner.chroot, _arch, self._owner.distgit) == _arch:
                    arch = _arch
                    break

            if chroot['state'] == 'skipped':
                self.skip_archs |= {arch}
                continue

            if all([failed_only, chroot['state'] not in ['failed', 'canceled']]):
                continue

            if not chroot.result_url:
                chroot.result_url = f'{build.repo_url}/srpm-builds/{self.build_id:08}/'

            self._collect_data(f"{dest}/{self._owner.name}/{self.build_status}/{chroot.name}",
                               chroot.result_url,
                               logs_only)

    def clean(self):
        """Infrastructure specific clean method"""
        self._owner.copr_wrapper(self._owner.client.package_proxy.delete,
                                 self._owner.ownername,
                                 self._owner.name,
                                 self.src_pkg_name)

    def location(self):
        """Get URL of the project"""
        base = f"{self._owner.client.config['copr_url']}"
        base = f'{base}/coprs/{self._owner.ownername}/{self._owner.name}'
        return f'{base}/build/{self.build_id}'


class MpbCoprBackend(MpbBackend):
    """Mass pre-build COPR back-end

        Implement COPR specific functionalities.

    Attributes:
        client: The COPR client instance

    """

    def _init_copr_config(self, config):
        default_conf = {}
        config.setdefault('copr', {})
        config['copr'].setdefault('config', '')
        config.setdefault('checker', {})
        config['checker'].setdefault('copr', {})

        files = Path('/etc/mpb/copr.conf.d/').glob('*')

        for fpath in files:
            if fpath.is_file():
                with open(str(fpath), 'r', encoding='utf-8') as file:
                    default_conf.update(yaml.safe_load(file))

        if not default_conf:
            self._logger.warning('Default configurations not found in /etc/mpb/copr.conf.d/')

        files = Path(Path.home() / '.mpb' / 'copr.conf.d').glob('*')
        for fpath in files:
            if fpath.is_file():
                with open(str(fpath), 'r', encoding='utf-8') as file:
                    self._logger.info(f'{self.name} Loading {str(fpath)}')
                    default_conf.update(yaml.safe_load(file))

        if not default_conf:
            raise FileNotFoundError('Default COPR configurations for MPB not found')

        checker_conf = default_conf.copy()
        # Update the default configuration with user inputs
        default_conf.update(config['copr'])
        checker_conf.update(config['checker']['copr'])

        # Now save the new configuration
        config['copr'] = default_conf
        config['checker']['copr'] = checker_conf

        accepted = ['all', 'revdeps']
        config['copr']['background'] = config['copr']['background'].lower()
        if config['copr']['background'] not in ['all', 'revdeps']:
            bg_val = config['copr']['background']
            raise ValueError(f'copr: background: wrong value {bg_val}, should be in {accepted}')

        if isinstance(config['copr']['disable_background'], str):
            config['copr']['disable_background'] = config['copr']['disable_background'].split()

        timeout = config['copr']['timeout']
        if int(timeout) < 0:
            self._logger.warning(f'Timeout is forced to 115200 due to invalid value {timeout}')
            timeout = 115200

        config['copr']['timeout'] = f'{timeout}'

    def __init__(self, database, logger, config):
        """Call the super class, and initialize the COPR client"""
        super().__init__(database, logger, config)
        copr_conf = config_from_file()

        self._init_copr_config(self.config)
        if self.config['copr']['config']:
            path = Path(self.config['copr']['config'])
            if path.is_file():
                copr_conf = config_from_file(str(path))
                self.config['checker']['copr']['config'] = self.config['copr']['config']

        self.client = Client(copr_conf)
        self.build_list = None
        self._monitoring = threading.Lock()
        self._next_check = 0
        self.reset_monitor()
        self.prepared = False
        self.ownername = self.client.base_proxy.auth_username()
        if 'ownername' in self.config['copr']:
            self.ownername = self.config['copr']['ownername']

        # Check that connection works in principle
        self.reconnect(5)

    def _get_package_class(self):
        """Return the package class to be used internally"""
        return MpbCoprPackage

    def prepare(self):
        """Setup the COPR project and call super class method"""
        if self.prepared:
            return

        accepted_chroots = get_accepted_chroots(self.chroot, self.archs, self.distgit)

        # Create a new project in COPR for this mass rebuild
        try:
            cconf = self.config['copr']
            self.copr_wrapper(self.client.project_proxy.add,
                              self.ownername,
                              self.name,
                              accepted_chroots,
                              description=cconf['description'],
                              instructions=cconf['instructions'],
                              homepage=cconf['homepage'],
                              contact=cconf['contact'],
                              additional_repos=cconf['additional_repos'],
                              unlisted_on_hp=cconf['unlisted_on_hp'],
                              enable_net=cconf['enable_net'],
                              persistent=cconf['persistent'],
                              auto_prune=cconf['auto_prune'],
                              use_bootstrap_container=cconf['use_bootstrap_container'],
                              devel_mode=cconf['devel_mode'],
                              delete_after_days=cconf['delete_after_days'],
                              multilib=cconf['multilib'],
                              module_hotfixes=cconf['module_hotfixes'],
                              bootstrap=cconf['bootstrap'],
                              bootstrap_image=cconf['bootstrap_image'],
                              isolation=cconf['isolation'],
                              fedora_review=cconf['fedora_review'],
                              appstream=cconf['appstream'],
                              runtime_dependencies=cconf['runtime_dependencies'],
                              packit_forge_projects_allowed=cconf['packit_forge_projects_allowed'],
                              )

            for c in accepted_chroots:
                additional_packages = []
                if "additional_packages" in self.config["copr"]:
                    add_pkgs_config = self.config["copr"]["additional_packages"]
                    additional_packages = add_pkgs_config.get(
                        "all", []
                    ) + add_pkgs_config.get(c, [])

                with_opts = []
                if "with_opts" in self.config["copr"]:
                    with_opts_config = self.config["copr"]["with_opts"]
                    with_opts = with_opts_config.get("all", []) + with_opts_config.get(
                        c, []
                    )
                self.copr_wrapper(
                    self.client.project_chroot_proxy.edit, self.ownername, self.name, c,
                    additional_packages=additional_packages, with_opts=with_opts
                )

        except exceptions.CoprRequestException as exc:
            if not exc.result['error'].startswith('name:'):
                raise

        self.prepared = True
        super().prepare()

    def clean(self):
        """Execute super class method and destroy COPR project"""
        super().clean()

        stop_builds = False

        while True:
            if stop_builds:
                size = len(self.packages)
                for pkg in self.packages:
                    pkg.check_status(False)
                    pkg.cancel()
                    pos = self.packages.index(pkg)
                    self._logger.debug(f'Stopped package build {pos}/{size}: {pkg.name}')

            try:
                self.copr_wrapper(self.client.project_proxy.delete, self.ownername, self.name)
                self._logger.warning(f'Deleted project {self.name}')
                break
            except exceptions.CoprRequestException as exc:
                if any(['can not delete build' in exc.result['error'],
                        'still running' in exc.result['error']]):
                    stop_builds = True
                    continue
                raise

    def reset_monitor(self):
        """Force monitor to get the build list on next run"""
        with self._monitoring:
            self._next_check = datetime.now() - timedelta(days=1)

    def copr_wrapper(self, func, *args, **kwargs):
        """Wrapper to copr methods for retrial"""
        while True:
            try:
                return func(*args, **kwargs)
            except exceptions.CoprNoResultException:
                break
            except (exceptions.CoprRequestException,
                    exceptions.CoprTimeoutException) as exc:
                if any([exc.result['error'].startswith('Unable to connect'),
                        exc.result['error'].startswith('Gateway Timeout')]):
                    self.reconnect(CONNECTION_TRIALS)
                    continue
                self._logger.debug(f'Got {exc} while executing {func} with {args}, {kwargs}')
                raise

        return None

    def reconnect(self, trials):
        """Reconnect to remote, with jitter between retrials, to avoid overloading the server"""
        max_sleep_time = 512
        base_sleep_time = 2
        while trials > 0:
            # Clear connection cache
            self.client.base_proxy.auth.make(reauth=True)
            try:
                self.client.base_proxy.auth_check()
                break
            except exceptions.CoprRequestException as exc:
                if all([exc.result['error'].startswith('Unable to connect'), trials > 0]):
                    # After 5 attempts, every new attempts will be done every ~6.5 min on average
                    try:
                        time.sleep(random.randint(base_sleep_time / 2, base_sleep_time))
                    except KeyboardInterrupt:
                        raise ValueError(
                                f'Failed to connect to remote: {exc.result["error"]}'
                                ) from exc

                    base_sleep_time = min(base_sleep_time * 2, max_sleep_time)
                    trials -= 1
                    continue
                raise ValueError(f'Failed to connect to remote: {exc.result["error"]}') from exc

    def monitor_project(self):
        """Get the whole project's package data"""
        with self._monitoring:
            if datetime.now() > self._next_check:
                build_list = self.copr_wrapper(self.client.build_proxy.get_list,
                                               self.ownername,
                                               self.name,
                                               )
                if not build_list:
                    return False
                self.build_list = build_list
                self._next_check = datetime.now() + timedelta(seconds=10)

        return True

    def location(self):
        """Get URL of the project"""
        return f"{self.client.config['copr_url']}/coprs/{self.ownername}/{self.name}"

    def can_queue(self):
        """COPR can queue builds to existing batches"""
        return True
