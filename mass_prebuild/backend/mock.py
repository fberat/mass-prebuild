""" Mock back-end implementation

    This module provides a back-end that executes builds using mock.
"""

from pathlib import Path

import subprocess

from .package import MpbPackage
from .backend import MpbBackend
from .executor import MpbExecutor

_rpkg_strings = {
        'centos-stream': 'centpkg',
        'fedora': 'fedpkg',
        'rhel': 'rhpkg',
        }


class MockPackage(MpbPackage):
    """Mass pre-build mock package implementation

    Attributes:
        process: The subprocess used to build the package
    """

    def __init__(self, database, logger, owner, pkg_data):
        """Create a new mass pre-build package

        The internals of the package are populated with the given data, unless
        the pkg_id is given. In the later case, the data is collected out of
        the database, based on the given package ID.
        Args:
            database: The database where data needs to be got from and stored to
            owner: The build owning this package
            pkg_data: The package data as a dictionary

        Returns:
            A new package object, filled with the required data.

        Raises:
            ValueError: if no package ID is given but mandatory parameters are
                missing.
            ValueError: If the given package ID doesn't match any entry in the
                database.
        """
        super().__init__(database, logger, owner, pkg_data)

        self._process = {}
        self._pidfile = {}
        self._srpm = None
        self._srcdir = None

        if hasattr(self._owner, 'build_path'):
            self.post_init()

    def post_init(self):
        """Finalize members"""
        for arch in self.archs:
            root = f'{self._owner.distgit}-{self._owner.releasever}-{arch}'

            resultdir = f'result/{root}/{self.src_pkg_name}'
            resultdir = Path(f'{str(self._owner.build_path)}/{resultdir}')

            self._pidfile[arch] = Path(f'{str(resultdir)}/mock.pid')
            self._logger.debug(f'Set pidfile up for {arch} ({self._pidfile[arch]})')

        if self.src_type in ['distgit']:
            if all([not self._owner.tools['rpkg']['path'], self._owner.distgit in _rpkg_strings]):
                self._owner.tools['rpkg']['path'] = \
                    self._owner.executor.which(_rpkg_strings[self._owner.distgit])

            if not self._owner.tools['rpkg']['path']:
                raise RuntimeError(f'Unable to locate {_rpkg_strings[self._owner.distgit]}')

        if all([self.src_type in ['distgit', 'git'], not self._owner.tools['git']['path']]):
            raise RuntimeError('Unable to locate git')

    def _exec_commands(self, cmds):
        """Execute a list of commands and check their results"""

        for name, cmd in cmds.items():
            self._logger.debug(f'Executing {name} subprocess in {cmd["cwd"]}')

            result = self._owner.executor.exec(cmd['cmd'], cwd=cmd['cwd'])

            self._logger.debug(result.stdout)
            try:
                result.check_returncode()
            except subprocess.CalledProcessError:
                self.build_status = 'FAILED'
                return result.stdout

        return result.stdout

    def _build_distgit(self):
        """Infrastructure specific build method"""
        self._build_setup()

        cmds = {
                'clean': {
                    'cmd': ['rm', '-rf', 'src'],
                    'cwd': str(self._srcdir),
                    },
                'checkout': {
                    'cmd': [self._owner.tools['rpkg']['path'],
                            'co',
                            '-b', self._owner.committish,
                            '--anonymous',
                            self.src_pkg_name,
                            'src'],
                    'cwd': str(self._srcdir),
                    },
                'commit': {
                    'cmd': [self._owner.tools['git']['path'],
                            'reset', '--hard', self.committish],
                    'cwd': f'{str(self._srcdir)}/src',
                    },
                'srpm': {
                    'cmd': [self._owner.tools['rpkg']['path'], 'srpm'],
                    'cwd': f'{str(self._srcdir)}/src',
                    },
                }

        # Ultimately, if everything goes fine, the last string is the SRPM name and path
        stdout = self._exec_commands(cmds).splitlines()
        srpm = ""
        for line in stdout:
            if any([self.src_pkg_name not in line, "src.rpm" not in line]):
                continue
            srpm = line.split(':')[1]
        self._srpm = srpm.strip()

        self._logger.debug(f'Prepared {self._srpm}')
        self._build_any()

    def _build_file(self):
        """Infrastructure specific build method"""
        self._build_setup()

        self._srpm = self.src

        self._build_any()

    def _build_git(self):
        """Infrastructure specific build method"""
        self._build_setup()

        cmds = {
                'clean': {
                    'cmd': ['rm', '-rf', 'src'],
                    'cwd': str(self._srcdir),
                    },
                'checkout': {
                    'cmd': [self._owner.tools['git']['path'],
                            'clone',
                            '-b', self._owner.committish,
                            self.src,
                            'src'],
                    'cwd': str(self._srcdir),
                    },
                'commit': {
                    'cmd': [self._owner.tools['git']['path'],
                            'reset', '--hard', self.committish],
                    'cwd': f'{str(self._srcdir)}/src',
                    },
                'prep': {
                    'cmd': [self._owner.tools['spectool']['path'],
                            '-g',
                            '-C', f'{str(self._srcdir)}/src',
                            f'{self.src_pkg_name}.spec'],
                    'cwd': f'{str(self._srcdir)}/src',
                    },
                'srpm': {
                    'cmd': [self._owner.tools['rpkg']['path'], 'srpm'],
                    'cwd': f'{str(self._srcdir)}/src',
                    },
                }

        # Ultimately, if everything goes fine, the last string is the SRPM name and path
        stdout = self._exec_commands(cmds).splitlines()
        srpm = ""
        for line in stdout:
            if any([self.src_pkg_name not in line, "src.rpm" not in line]):
                continue
            srpm = line.split(':')[1]
        self._srpm = srpm.strip()
        self._logger.debug(f'Prepared {self._srpm}')

        self._build_any()

    def _build_script(self):
        """Infrastructure specific build method"""
        self._build_setup()
        self._build_any()

    def _build_url(self):
        """Infrastructure specific build method"""
        self._build_setup()

        srcdir = Path(self._srcdir / 'src')
        self._owner.executor.mkdir(srcdir)

        cmds = {
                'checkout': {
                    'cmd': ['wget', self.src],
                    'cwd': str(srcdir),
                    },
                }

        self._exec_commands(cmds)

        self._srpm = f'{str(srcdir)}/{self.src.split("/")[-1]}'

        self._build_any()

    def _build_setup(self):
        """Prepare build environment"""
        self.build_status = 'RUNNING'

        self._srcdir = Path(f'{str(self._owner.build_path)}/sources/{self.name}')
        self._owner.executor.mkdir(self._srcdir)

        cmd = [self._owner.tools['createrepo']['path'],
               '--update',
               f'{self._owner.build_path}/result']
        self._owner.executor.exec(cmd)

    def _build_any(self):
        """Infrastructure specific build method"""
        # pylint: disable = consider-using-with
        self.build_status = 'PENDING'
        for arch in self.archs:
            root = f'{self._owner.distgit}-{self._owner.releasever}-{arch}'

            resultdir = f'result/{root}/{self.src_pkg_name}'
            resultdir = Path(f'{str(self._owner.build_path)}/{resultdir}')
            self._owner.executor.mkdir(resultdir)

            cmd = [self._owner.tools['mock']['path'],
                   f'--resultdir={str(resultdir)}',
                   '--cleanup-after',
                   f'--target={arch}',
                   f'--uniqueext={self._owner.build_id}_{self.name}',
                   f'--root={root}',
                   f'--addrepo={self._owner.build_path}/result',
                   self._srpm]

            self._process[arch] = self._owner.executor.popen(cmd)

            # build_id only needs to be non-zero
            self.build_id += self._process[arch].pid
            self._logger.debug(f'Building {self.name}:{arch} ({self._process[arch].pid})')
            self._owner.executor.store_pid(self._process[arch].pid, self._pidfile[arch])
            self._logger.debug(f'PID: {self._owner.executor.cat(self._pidfile[arch])}')

        self.build_status = 'RUNNING'
        self._update_package()

    def _retrieve_status(self, arch):
        """Retrieve status from subprocess"""
        # pylint: disable = too-many-return-statements
        # Are we still attached to the process ?
        if arch in self._process:
            return self._process[arch].poll(), 0

        # Was the process ever started ?
        if not self._owner.executor.exists(self._pidfile[arch]):
            self._logger.debug(f"Can't find {self._pidfile[arch]}")
            return None, 1

        # Was termination already spotted ?
        pid = int(self._owner.executor.cat(self._pidfile[arch]) or "0")

        if pid <= 0:
            self._logger.debug(f"PID: {pid}")
            return pid, 0

        # Last resort, look for mock status from the result output
        root = f'{self._owner.distgit}-{self._owner.releasever}-{arch}'

        resultdir = f'result/{root}/{self.src_pkg_name}'
        resultdir = Path(f'{str(self._owner.build_path)}/{resultdir}')
        build_log = Path(resultdir / 'build.log')

        if not self._owner.executor.exists(build_log):
            self._logger.debug(f"Can't find {build_log}")
            return None, 0

        cmds = {
                'tail': {
                    'cmd': ['tail', '-1', 'build.log'],
                    'cwd': str(resultdir),
                    }
                }
        exit_status = self._exec_commands(cmds)

        if 'Child return code was:' not in exit_status:
            self._logger.debug("Can't find return code in build.log")
            return None, 0

        exit_status = int(exit_status.split()[-1] or "-1")

        if not exit_status:
            return 0, 0

        return -1, 0

    def _check_status(self):
        """Infrastructure specific build sub-routine"""
        pending = 0
        running = 0
        success = 0
        failed = 0
        max_count = 0

        for arch in self.archs:
            max_count += 1

            ret, is_pending = self._retrieve_status(arch)

            self._logger.debug(f"Status is {ret}, {is_pending}")

            if is_pending:
                pending += 1
                continue

            if all([ret is not None, ret != 0]):
                failed += 1
                if arch in self._pidfile:
                    self._owner.executor.store_pid(-1, self._pidfile[arch])

            if ret is None:
                running += 1

            if all([ret is not None, ret == 0]):
                success += 1
                if arch in self._pidfile:
                    self._owner.executor.store_pid(0, self._pidfile[arch])

        return pending, running, success, failed, max_count

    def check_status(self, check_needed=True):
        """Infrastructure specific build method"""
        if not self.build_id:
            return True

        status = self.build_status

        if status not in ['PENDING', 'RUNNING']:
            return True

        pending, running, success, failed, max_count = self._check_status()

        if max_count == success:
            status = 'SUCCESS'

        if running > 0:
            status = 'RUNNING'

        if pending > 0:
            status = 'PENDING'

        if failed > 0:
            if check_needed:
                status = 'CHECK_NEEDED'
            else:
                status = 'FAILED'

        if self.build_status != status:
            self._logger.debug(f"New status for {self.name} is {status}")

        self.build_status = status

        return True

    def cancel(self):
        """Infrastructure specific cancel method"""
        if self.build_status != 'SUCCESS':
            self.build_status = 'FAILED'

        if not self.build_id:
            return

        for arch in self.archs:
            if arch in self._process:
                self._process[arch].terminate()
                self._logger.debug(f'Terminating {self.name}:{arch} ({self.build_id})')
                try:
                    self._process[arch].terminate()
                    continue
                except PermissionError:
                    pass

            if not self._owner.executor.exists(self._pidfile[arch]):
                continue

            pid = int(self._owner.executor.cat(self._pidfile[arch]) or "0")

            if pid <= 0:
                continue

            # Check that the command associated to PID is actually ours
            cmds = {
                    'ps': {
                        'cmd': ['ps', '-p', str(pid), '-o', 'args'],
                        'cwd': str(self._owner.build_path),
                        }
                    }
            command = self._exec_commands(cmds)
            root = f'{self._owner.distgit}-{self._owner.releasever}-{arch}'

            resultdir = f'result/{root}/{self.src_pkg_name}'
            resultdir = Path(f'{str(self._owner.build_path)}/{resultdir}')

            if str(resultdir) not in command:
                continue

            cmds = {
                    'terminate': {
                        'cmd': ['sudo', 'kill', '-s', 'TERM', str(pid)],
                        'cwd': str(self._owner.build_path),
                        }
                    }
            command = self._exec_commands(cmds)

    def collect_data(self, dest, logs_only=False, failed_only=True):
        """Infrastructure specific data collector method"""
        self._logger.debug(f'Collecting data for {self.src_pkg_name}')

        for arch in self.archs:
            if failed_only:
                ret, is_pending = self._retrieve_status(arch)
                if any([is_pending, ret is None, ret == 0]):
                    continue

            suffix = f'{self._owner.distgit}-{self._owner.releasever}-{arch}/{self.src_pkg_name}'

            resultdir = Path(f'{str(self._owner.build_path)}/result/{suffix}')

            destdir = Path(f'{dest}/{self._owner.name}/{self.build_status}/{suffix}')
            destdir.mkdir(parents=True, exist_ok=True)

            if not self._owner.executor.exists(resultdir):
                continue

            files = self._owner.executor.ls(['-1', str(resultdir)]).splitlines()
            for file in files:
                if all([logs_only, any([file.endswith('.rpm'), ".tar." in file])]):
                    continue
                self._owner.executor.cp(f'{resultdir}/{file}', f'{destdir}/{file}')

    def clean(self):
        """Infrastructure specific clean method"""
        for arch in self.archs:
            root = f'{self._owner.distgit}-{self._owner.releasever}-{arch}'

            resultdir = f'result/{root}/{self.src_pkg_name}'
            resultdir = Path(f'{str(self._owner.build_path)}/{resultdir}')
            resultdir.mkdir(parents=True, exist_ok=True)

            cmd = [self._owner.tools['mock']['path'],
                   f'--uniqueext={self._owner.build_id}_{self.name}',
                   f'--root={root}',
                   '--bootstrap-chroot',
                   '--scrub=all',
                   ]
            self._owner.executor.exec(cmd)


class MockBackend(MpbBackend):
    """Mock backend implementation"""

    def __init__(self, database, logger, config, executor=None):
        """Call the super class, and initialize the mock build environment"""
        super().__init__(database, logger, config)

        self.executor = executor if executor else MpbExecutor(logger)

        self.tools = {
                'createrepo': {
                    'path': self.executor.which('createrepo_c'),
                    'mandatory': True
                    },
                'mock': {
                    'path': self.executor.which('mock'),
                    'mandatory': True
                    },
                'git': {
                    'path': self.executor.which('git'),
                    'mandatory': False
                    },
                'spectool': {
                    'path': self.executor.which('spectool'),
                    'mandatory': True
                    },
                'rpkg': {
                    'path': None,
                    'mandatory': False
                    },
                }

        self._srcdir = None

        for name, tool in self.tools.items():
            if any([not tool['mandatory'], tool['path']]):
                continue
            raise RuntimeError(f'Unable to locate {name} to process build')

        home_dir = self.executor.home()
        self.build_path = Path(home_dir) / ".mpb" / "mock_build" / f"{self.build_id}"

        self.executor.mkdir(self.build_path)
        result_base = Path(self.build_path) / 'result'
        self.executor.mkdir(result_base)

        for pkg in self.packages:
            pkg.post_init()

    def clean(self):
        """Execute super class method and destroy project data"""
        super().clean()

        size = len(self.packages)
        for pkg in self.packages:
            pkg.check_status(False)
            pkg.cancel()
            pkg.clean()
            pos = self.packages.index(pkg)
            self._logger.debug(f'Stopped package build {pos}/{size}: {pkg.name}')

        if self.build_path:
            self.executor.rm(['-rf', str(self.build_path)])

    def location(self):
        """Get URL of the project"""
        return f"{str(self.build_path)}"

    def _get_package_class(self):
        """Return the package class to be used internally"""
        return MockPackage
