# pylint: disable = missing-module-docstring
from importlib.metadata import version, PackageNotFoundError

VERSION_INFO = (1, 6, 0)
__version__ = '.'.join(str(c) for c in VERSION_INFO)

try:
    __version__ = version("mass-prebuild")
except PackageNotFoundError:
    # package is not installed
    pass

VERSION = __version__
