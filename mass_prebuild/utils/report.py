""" MPB report generator

    This script generates a status summary for a given MPB build.
"""
import argparse
from pathlib import Path
import sys

import argcomplete
import yaml

from mass_prebuild import VERSION

from mass_prebuild.backend import copr, dummy
from mass_prebuild.backend.backend import MAIN_PKG, ALL_DEP_PKG
from mass_prebuild.backend.db import MpbDb
from mass_prebuild.backend.log import MpbLog

def handle_exception(exc_type, exc_value, exc_traceback):
    """Log exception into a file to ease debugging"""
    if exc_type == SystemExit:
        return

    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger().error('Stopping due to Uncaught exception !')
    old_level = logger().level
    logger().level = -2
    logger().logger.critical("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
    logger().level = max(min(old_level, 5), -2)
    logger().error(f'Exception log stored in {str(Path(Path.home() / ".mpb" / "mpb-report.log"))}')

    release_database()


def logger():
    """Setup and return the logger"""
    if not hasattr(logger, "logger"):
        logger.logger = MpbLog(name="mpb-report")

    return logger.logger


def database(config=None):
    """Setup and return the database"""
    if not hasattr(database, "database"):
        database.database = None

    if config:
        database.database = MpbDb(path=config['database'], verbose=config['verbose'])

    return database.database


def release_database():
    """Release the database locks"""
    if not hasattr(release_database, 'released'):
        release_database.released = False

    if all([database() is not None, not release_database.released]):
        database().release()
        release_database.released = True


def validate_database(config):
    """Validate the database"""
    if not database(config):
        config['validity'] = False


def validate_build_id(config):
    """Validate build ID"""
    if not config['build_id']:
        config['validity'] = False
        return

    if not database():
        print('No valid database to check for build ID')
        config['validity'] = False
        config['build_id'] = 0
        return

    build = database().build_by_id(config['build_id'])

    if not build:
        print(f'Build ID doesn\'t match any known ID {config["build_id"]}')
        config['build_id'] = 0
        config['validity'] = False


def validate_backend(user_config):
    """Validate the provided backend"""
    if any([not database(), user_config['build_id'] <= 0]):
        user_config['validity'] = False
        return

    # First try to get the backend from the database
    build = database().build_by_id(user_config['build_id'])
    build_config = yaml.safe_load(build['config'])

    backend = 'unknown'

    if 'backend' in build_config:
        backend = build_config['backend']

    if backend == 'unknown':
        backend = user_config['backend']
    else:
        user_config['backend'] = backend

    if backend not in ['copr', 'dummy']:
        print('Unknown back-end in database, assuming COPR')
        user_config['backend'] = 'copr'


def backends():
    """List of available back-ends"""
    return {'dummy': dummy.DummyBackend, 'copr': copr.MpbCoprBackend}


def current_backend(config=None):
    """Global backend accessors"""
    if not hasattr(current_backend, "backend"):
        current_backend.backend = None

    if config:
        current_backend.backend = backends()[config['backend']](database(), logger(), config)

    return current_backend.backend


def parse_args():
    """Setup argument parser"""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--verbose',
        '-V',
        action='count',
        help='increase output verbosity, may be provided multiple times',
    )
    parser.add_argument(
        '--version',
        '-v',
        action='version',
        version=f'%(prog)s {VERSION}',
        help='show the version number and exit',
    )
    parser.add_argument('--buildid', '-b', type=int, help='build ID to work on')
    parser.add_argument(
        '--output',
        '-o',
        nargs='?',
        default=None,
        const='',
        help='''By default, output is printed to STDOUT.
        Specify this option to print output to ./{Build name}-report.md file.
        The file name can be optionally specified.''',
    )

    argcomplete.autocomplete(parser)

    return parser.parse_args()


def populate_config():
    """Get config from file if given and initialize default values"""
    home_path = Path(Path.home() / '.mpb')
    home_path.mkdir(parents=True, exist_ok=True)

    args = parse_args()

    config = {}
    config['build_id'] = args.buildid or config.setdefault('build_id', 0)
    config['verbose'] = args.verbose or config.setdefault('verbose', 0)
    config['output'] = args.output
    config.setdefault('database', str(Path(home_path / 'share' / 'mpb.db')))

    config.setdefault('backend', 'copr')
    config.setdefault('base_build_id', 0)
    config.setdefault('stage', -1)
    config.setdefault('chroot', 'fedora-rawhide')
    config.setdefault('archs', 'x86_64')
    config.setdefault('skip_archs', '')
    config.setdefault('cancel', False)
    config.setdefault('clean', False)
    config.setdefault('clean_db', False)
    config.setdefault('enable_priorities', False)
    config.setdefault('collect', False)
    config.setdefault('data', str(Path(home_path / 'data')))
    config.setdefault('dnf_conf', '')
    config.setdefault('collect_list', 'failed')
    config.setdefault('list', False)
    config.setdefault('info', False)
    config.setdefault('list_packages', False)
    config.setdefault('retry', 0)
    config.setdefault('collect_list_override', False)
    config['validity'] = True

    return config


def cleanup(config):
    """Remove unnecessary elements"""
    config.pop('base_build_id', None)
    config.pop('revdeps', None)
    config.pop('cancel', None)
    config.pop('clean', None)
    config.pop('clean_db', None)
    config.pop('enable_priorities', None)
    config.pop('collect', None)
    config.pop('list', None)
    config.pop('list_packages', None)
    config.pop('info', None)
    config.pop('collect_list_override', None)
    config.pop('stage', None)
    config.pop('copr', None)
    config.pop('rebuild', None)

    if not config['verbose']:
        config.pop('verbose', None)

    if not config['skip_archs']:
        config.pop('skip_archs', None)

    if not config['retry']:
        config.pop('retry', None)

    if not config['dnf_conf']:
        config.pop('dnf_conf', None)

    if config['backend'] == 'copr':
        config.pop('backend', None)

    if config['collect_list'] == 'failed':
        config.pop('collect_list', None)

    config.pop('output', None)
    config.pop('checker', None)
    config.pop('status', None)
    config.pop('validity', None)

    if 'name' not in config:
        config['name'] = f'mpb.{config["build_id"]}'

def overall_status():
    """Print a status of the build"""
    report = current_backend().prepare_report(ALL_DEP_PKG)

    if not report[0]:
        return 'No packages built'

    ret = f'{" "*4}{report[1]} out of {report[0]} builds are done.'
    if report[2]:
        ret += f'\n{" "*4}Not started: {report[2]:<10}'
    if report[3]:
        ret += f'\n{" "*4}Pending: {report[3]:<10}'
    if report[4]:
        ret += f'\n{" "*4}Running: {report[4]:<10}'
    if report[5]:
        ret += f'\n{" "*4}Success: {report[5]:<10}'
    if report[6]:
        ret += f'\n{" "*4}Under check: {report[6]:<10}'
    if report[7]:
        ret += f'\n{" "*4}Manual confirmation needed: {report[7]:<10}'
    if report[8]:
        ret += f'\n{" "*4}Failed: {report[8]:<10}'

    return ret

def packages(status='main'):
    """Print packages with the given status, `main` being a special case"""

    if status == 'main':
        plist = [p for p in current_backend().packages if p.pkg_type & MAIN_PKG]
    else:
        plist = [p for p in current_backend().packages if all([p.pkg_type & ALL_DEP_PKG,
                                                               p.build_status == status.upper()])]


    ret = ""

    for pkg in plist:
        if ret:
            ret += "\n"
        ret += f'{" "*4}{pkg.name}: \n'
        ret += f'{" "*4}{" "*4}Source: ({pkg.src_type}) {pkg.src}\n'
        if all([pkg.nvr, pkg.src_type not in ['file', 'script']]):
            ret += f'{" "*4}{" "*4}NVR: {pkg.nvr}\n'
        if all([pkg.committish, pkg.src_type not in ['file', 'script']]):
            ret += f'{" "*4}{" "*4}Commit/branch: {pkg.committish}'

    return ret

def build_location():
    """Retrieve URL from the project"""
    return current_backend().location()


def main():
    """Main function

    Setup argument parser, initialize default configuration, validates user
    input and execute the mass pre-build build.
    """
    logger()
    sys.excepthook = handle_exception

    config = populate_config()

    validate_database(config)
    validate_build_id(config)
    validate_backend(config)

    if not config['validity']:
        print('Invalid parameters')
        release_database()
        return 1

    logger().info('Setting up the back-end')
    try:
        current_backend(config)
    except ValueError as exc:
        print(exc)
        release_database()
        return 1

    new_config = current_backend().config

    cleanup(new_config)

    if config['output']:
        if config['output'] == '':
            config['output'] = f'./{new_config["name"]}-report.md'

        path = Path(config['output'])
        print(f'Saving report: {str(path)}')

    report = f"""# Mass-prebuild {new_config["name"]} (ID:{new_config["build_id"]})

This report was generated using mpb-report {VERSION}

## General information

Build location: {build_location()}
Chroot: {new_config["chroot"]}
Tested architectures: {new_config["archs"]}

Main packages tested:

{packages()}

## Overall status

{overall_status()}

## List of failed packages

{packages('failed')}

## List of packages with unknown status

{packages('unconfirmed')}
    """

    if config['output']:
        with open(str(path), 'w', encoding='utf-8') as file:
            print(report, file=file)
    else:
        print(report)

    release_database()

    return 0
