""" Retrieve reverse dependencies based on a list of packages

    This module provides facilities in order to calculate the reverse
    dependencies of a provided list of packages. The list is segregated based
    on the architecture and the corresponding distribution's package
    repositories that should be looked at.
    The module can either be used stand-alone (as a plain application) or
    imported into another module.
"""
# pylint: disable = import-error

from math import floor
from pathlib import Path

import libdnf5
import koji
import yaml

from mass_prebuild.backend.db import all_archs, authorized_archs

arch_table = {
    'aarch64': {'repo_arch': 'aarch64', 'search_arch': 'aarch64'},
    'i386': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'i586': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'i686': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'ppc64le': {'repo_arch': 'ppc64le', 'search_arch': 'ppc64le'},
    's390x': {'repo_arch': 's390x', 'search_arch': 's390x'},
    'x86': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'x86_64': {'repo_arch': 'x86_64', 'search_arch': 'x86_64'},
}


class MpbWhatrequires:
    """Mass prebuild package reverse dependencies retrieval

      This class implements all the methods needed in order to retrieve
      information about reverse dependencies for a given package, on a given
      distribution for a given architecture.
    """
    # pylint: disable = too-many-instance-attributes

    def __init__(self,
                 logger_in,
                 distrib='fedora',
                 releasever='rawhide',
                 archs=None):
        """Create a new MpbWhatrequires entity"""

        self.logger = logger_in
        self.distrib = distrib
        self.releasever = releasever
        self.archs = archs

        self._koji_session = None
        self._dnf_session = {}
        self.metalinks = {}
        self.logger.debug('Using DNF5 API')
        self._load_default_repo_conf()

    @property
    def archs(self):
        """Returns the set of supported archs"""
        return self._archs

    @archs.setter
    def archs(self, new_arch):
        """Set the set of supported archs

            The set of unsupported archs is automatically updated by removing
            values from this supported arch set.
        Args:
            new_arch: The set to set

        Raises:
            TypeError: The input is not a set
            ValueError: The input contains unsupported values
        """
        if 'all' in new_arch:
            self._archs = all_archs.copy()
            return

        if isinstance(new_arch, str):
            new_arch = {new_arch}

        if not isinstance(new_arch, set):
            raise TypeError(f'Set of arch must be given ({new_arch})')

        if not all(elem in authorized_archs for elem in new_arch):
            raise ValueError(f'Archs should be in {authorized_archs}')

        self._archs = new_arch.copy()

    def load_repo_config(self, file_in):
        """Get custom repositories out of provided path"""
        if not file_in:
            return

        if not Path(file_in).is_file():
            raise ValueError(f'Can\'t find DNF config: {file_in}')

        with open(file_in, 'r', encoding='utf-8') as file:
            self.metalinks |= yaml.safe_load(file)
            self.logger.info(f'Loaded {file_in}')
            self.logger.debug(self.metalinks)

    @property
    def metalinks(self):
        """The metalinks property."""
        return self._metalinks

    @metalinks.setter
    def metalinks(self, meta_in=None):
        """Provide custom url/metalinks"""
        self._metalinks = {}

        if meta_in:
            self._metalinks.update(meta_in)

    def _load_default_repo_conf(self):
        """Load default metalinks configuration"""
        files = Path('/etc/mpb/repo.conf.d/').glob('*')
        default_conf = False

        for file in files:
            if file.is_file():
                default_conf = True
                self.load_repo_config(str(file))

        if not default_conf:
            self.logger.warning('Default configurations not found in /etc/mpb/repo.conf.d/')

        files = Path(Path.home() / '.mpb' / 'repo.conf.d' / '').glob('*')
        for file in files:
            if file.is_file():
                default_conf = True
                self.load_repo_config(str(file))

        if not default_conf:
            self.logger.error('Default repo configurations for MPB not found')

    @property
    def koji_session(self):
        """Global access to koji session"""
        if self._koji_session:
            return self._koji_session

        meta = self.metalinks[self.distrib]
        if self.releasever in meta:
            if 'koji' in meta[self.releasever]:
                meta = meta[self.releasever]

        self._koji_session = koji.ClientSession(meta['koji'])

        return self._koji_session

    def _add_repo(self, dbase, meta, src_type, arch):
        """Add a repo (src or bin) from meta to base"""
        # DNF repos don't have classical attributes ...
        default = self.metalinks[self.distrib]
        if all([f'{src_type}-repos' not in meta, f'{src_type}-repos' not in default]):
            return
        if all(['base' not in meta, 'base' not in default]):
            return

        if 'base' not in meta:
            meta_base = default['base']
        else:
            meta_base = meta['base']

        releasebase = self.releasever.split('.')[0]

        if f'{src_type}-repos' not in meta:
            meta = default

        repo_arch = arch_table[arch]['repo_arch']

        for src_repo in meta[f'{src_type}-repos']:
            idx = meta[f'{src_type}-repos'].index(src_repo)
            repo = dbase.get_repo_sack().create_repo(f'custom_{arch}_{str(idx)}_{src_type}')

            link = meta_base.format(releasebase=releasebase, release=self.releasever)
            link += src_repo.format(release=self.releasever, arch=repo_arch)

            self.logger.debug(f'Using {link} for the repository')

            if 'metalink' in meta_base:
                repo.get_config().metalink = link
            else:
                repo.get_config().baseurl = link

    def _dnf_base(self, arch='x86_64', srcbin='all'):
        """DNF base instances"""
        if arch not in self._dnf_session:
            self._dnf_session[arch] = {}

        if srcbin not in self._dnf_session[arch]:
            base = libdnf5.base.Base()
            # Recent version of DNF seem to silently filter out providers
            # that don't match base.conf.arch
            base.get_config().arch = arch_table[arch]['search_arch']
            meta = self.metalinks[self.distrib]

            if self.releasever in meta:
                meta = meta[self.releasever]

            base.setup()

            # Setup the repositories
            for src_type in ['src', 'bin', arch]:
                self._add_repo(base, meta, src_type, arch)

            # By default disable everything
            all_repos = libdnf5.repo.RepoQuery(base)
            for repo in all_repos:
                repo.disable()

            for src_type in ['src', 'bin', arch]:
                if any([srcbin == 'all', srcbin == src_type]):
                    # Enable binaries
                    binary_repos = libdnf5.repo.RepoQuery(base)
                    binary_repos.filter_id(f'custom*{src_type}', libdnf5.common.QueryCmp_GLOB)
                    for repo in binary_repos:
                        repo.enable()

            base.get_repo_sack().load_repos(libdnf5.repo.Repo.Type_AVAILABLE)

            self._dnf_session[arch][srcbin] = base

        return self._dnf_session[arch][srcbin]

    def dnf_base(self, arch='x86_64'):
        """DNF base instances for both binaries and sources"""
        return self._dnf_base(arch, 'all')

    def dnf_base_src(self, arch='x86_64'):
        """DNF base instances for both sources only"""
        return self._dnf_base(arch, 'src')

    def dnf_base_bin(self, arch='x86_64'):
        """DNF base instances for both binaries only"""
        return self._dnf_base(arch, 'bin')

    def _get_binaries(self, pkg, arch):
        """Get all the binary packages associated to a component"""
        provides = set()
        base = self.dnf_base(arch)

        if pkg.get_sourcerpm():
            source_pkgs = libdnf5.rpm.PackageQuery(base)
            source_pkgs.filter_available()
            source_pkgs.filter_name(pkg.source_name)
            source_pkgs.filter_arch('src')
            source_pkgs.filter_latest_evr()

            for src in source_pkgs:
                return self._get_binaries(src, arch)
        else:
            # Walk through provides, and collect all binary packages that match them
            pkgs = libdnf5.rpm.PackageQuery(base)
            pkgs.filter_available()
            pkgs.filter_arch('src', libdnf5.common.QueryCmp_NEQ)
            pkgs.filter_provides(pkg.get_provides())
            pkgs.filter_latest_evr()
            provides.update(set(pkgs))

        return provides

    def _get_sourcerpm(self, pkg, arch):
        """Get the sourcerpm from a given package, whether it is a binary package or a component"""
        sourcerpm = ''

        if pkg.get_sourcerpm():
            sourcerpm = pkg.get_sourcerpm()
        else:
            provides = self._get_binaries(pkg, arch)
            # From these binary packages, stop at the first that has a source name that matches
            for bin_pkg in provides:
                if bin_pkg.get_source_name() != pkg.get_name():
                    continue
                # Extract sourcerpm from this match and be done with it
                sourcerpm = bin_pkg.get_sourcerpm()
                break

        return sourcerpm

    def get_last_build(self, package, arch='x86_64'):
        """Get the source package name and commit ID from latest version of a
        package.

        This function first retrieves the latest available package matching the
        name provided, and reconstruct the NVR of the corresponding source package.
        Based on this NVR, it asks Koji to give the build information associated to
        it, and extracts the commit ID out of this data.

        Args:
            package: The package to get the tuple from
            distrib: The distribution (fedora, epel, centos ...)
            releasever: The release version for the distribution. e.g for fedora:
            36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.
            arch: The architecture for the packages (x86_64, aarch64 ...).

        Returns:
            source name, commit ID, nvr, build source
        """
        base = self.dnf_base(arch)

        # Look for components
        pkgs = libdnf5.rpm.PackageQuery(base)
        pkgs.filter_available()
        pkgs.filter_name(package)
        pkgs.filter_arch('src')
        pkgs.filter_latest_evr()

        # In case there is no component found with that name, look for providers,
        # whether they are binary packages or virtual provides
        other_pkgs = libdnf5.rpm.PackageQuery(base)
        other_pkgs.filter_available()
        other_pkgs.filter_provides(package)
        other_pkgs.filter_arch('src', libdnf5.common.QueryCmp_NEQ)
        other_pkgs.filter_latest_evr()

        pkgs |= other_pkgs

        if pkgs.empty():
            return package, '', None, ''

        # Prefer packages that provide the sourcerpm when available
        pkgs = sorted(pkgs, key=lambda x: (x.get_sourcerpm() is None, x.get_name()))

        ret = {}

        for pkg in pkgs:
            name = pkg.get_source_name() or pkg.get_name()
            if name in ret:
                continue

            ret[name] = {}
            ret[name]['name'] = name
            ret[name]['nvr'] = self._get_sourcerpm(pkg, arch).removesuffix('.src.rpm')

        ret = ret.get(package, ret[next(iter(ret))])

        ret['committish'] = None
        ret['source'] = self.distrib
        try:
            build_info = self.koji_session.getBuild(ret['nvr'], strict=True)
            if build_info['source'] is not None:
                ret['committish'] = build_info['source'].split('#')[1]
                ret['source'] = build_info['source'].split('#')[0]
        except koji.GenericError:
            self.logger.info(f'Package {name} ({ret["nvr"]}) not found in koji')

        return ret['name'], ret['committish'], ret['nvr'], ret['source']

    def __get_reverse_deps(self, packages, arch=None) -> dict:
        """Returns a dict of package reverse dependencies.

        From a list of a given package, request dnf to retrieve the packages that
        require at least one of the elements of this list from its BuildRequires
        field.

        Args:
            packages: The list of packages to get reverse dependencies of.
            distrib: The distribution (fedora, epel, centos ...)
            releasever: The release version for the distribution. e.g for fedora:
            36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.
            arch: The architecture for the packages (x86_64, aarch64 ...).

        Returns:
            A dict of packages that depend on the input for their build, on a
            dedicated architecture.
            These packages are either architecture independent (noarch), or specific to
            the architecture requested.
        """
        # pylint: disable= too-many-locals, too-many-statements, too-many-branches
        if not arch:
            return {}

        ret = {}

        base = self.dnf_base(arch)
        binary_pkgs = libdnf5.rpm.PackageSet(base)

        for pkg_str in packages:
            # First, assume this is a binary package or a virtual provide.
            # Get a list of binary package objects:
            pkg_binary_pkgs = libdnf5.rpm.PackageQuery(base)
            pkg_binary_pkgs.filter_available()
            pkg_binary_pkgs.filter_provides(pkg_str)
            pkg_binary_pkgs.filter_arch('src', libdnf5.common.QueryCmp_NEQ)
            pkg_binary_pkgs.filter_latest_evr()

            binary_pkgs |= pkg_binary_pkgs

            # Next, assume it is a component name.
            # Get a list of source package objects:
            source_pkgs = libdnf5.rpm.PackageQuery(base)
            source_pkgs.filter_available()
            source_pkgs.filter_name(pkg_str)
            source_pkgs.filter_arch('src')
            source_pkgs.filter_latest_evr()

            for source_pkg in source_pkgs:
                # Get a list of all binary packages from these source packages and add it to the
                # main list:
                source_pkg_binary_pkgs = libdnf5.rpm.PackageQuery(base)
                source_pkg_binary_pkgs.filter_available()
                source_pkg_binary_pkgs.filter_provides(source_pkg.get_provides())
                source_pkg_binary_pkgs.filter_arch('src', libdnf5.common.QueryCmp_NEQ)
                source_pkg_binary_pkgs.filter_latest_evr()

                binary_pkgs |= source_pkg_binary_pkgs

        # This package set will be used to filter out calculated reverse
        # dependencies that are part of the given list of packages. That may
        # happen if we have multiple main packages that may depend on each
        # other.
        pkg_set = {p.get_source_name() or p.get_name() for p in binary_pkgs}

        # Now that we have the list of all binary packages to use as required
        # set, calculate the corresponding reverse dependencies. By using a
        # list of package objects, virtual provides/requires work Don't filter
        # for latest here, as that may make DNF to filter out some virtual
        # provides.
        revdeps = libdnf5.rpm.PackageQuery(base)
        revdeps.filter_requires(binary_pkgs)

        # Prefer packages that provide the sourcerpm when available
        revdeps = sorted(list(revdeps), key=lambda x: (x.get_sourcerpm() is None))
        candidates = {p.get_source_name() or p.get_name(): p for p in revdeps
                      if all([p.get_name() not in pkg_set, p.get_source_name() not in pkg_set])}

        self.logger.info(
                f'Found {len(candidates)} candidates ({len(revdeps)} binary packages to process).')

        for pkg in candidates.values():
            src_name = pkg.get_source_name() or pkg.get_name()

            # Don't add package in reverse dependencies if
            # they are already in the main package list
            if src_name in pkg_set:
                continue

            ret[src_name] = {}
            deps = set()
            provides = set()

            subpkgs = [p for p in revdeps if any([p.get_source_name() == src_name,
                                                  p.get_name() == src_name])]
            for subpkg in subpkgs:
                deps.update(dep.get_name() for dep in subpkg.get_regular_requires())
                provides.update(prov.get_name() for prov in subpkg.get_provides())

            ret[src_name]['deps'] = list(deps)
            ret[src_name]['provides'] = list(provides)
            ret[src_name]['archs'] = [arch]

            if not len(ret) % 10:
                self.logger.debug(f'Got {len(ret)} components.')
                print(f'Got {len(ret)} components.', end='\r')

        ret = dict(sorted(ret.items()))
        self.logger.info(f'Got {len(ret)} components.')
        return ret

    def _increase_priority(self, pkg, pkg_dict):
        """ Increase the value of the priority field based on the priority of
            the dependencies of the given package.

            Note: Higher value means lower priority (priority 1 is built before
            priority 2, etc...)

            This algorithm isn't meant to be optimum, but only to provide a
            fast forward solution for the ordering. An algorithm looking for
            the optimum order would require much more effort to implement, with
            no clear benefit for the current purpose of the mass-prebuilder:
            warning a developer for build failure on others packages due to its
            own package changes.

            In the advent the mass-prebuilder becomes not only a tool
            facilitating A/B testing but a mass builder, this ordering need to
            be improved (as it would become a major feature).

            For reference, it has been suggested to look at feedback arc set,
            for an improved solution.

            Yet, be aware that there is no guaranty that all circular
            dependencies can be broken down.
        """
        increased_prio = False

        for dep in pkg['real_deps']:
            provider = pkg_dict[dep]

            if provider['name'] == pkg['name']:
                continue

            # Break circular dependencies.
            # Use an arbitrary discriminator to flatten the dependency
            # graph. The more occurrences can be found in the
            # dependencies, the highest the priority for this package
            # is.
            # It will just continue when find real circular dependencies
            # to avoid misjudgment like:
            #     C -> B, D -> B, B -> A, B['occurrence'] > A['occurrence']
            if all([provider['occurrence'] < pkg['occurrence'],
                    pkg['name'] in provider['real_deps']]):
                continue

            # For equal occurrences, we only break the loop it the package is alone in
            # its priority list
            current_prio = [p for p in pkg_dict.values() if p['priority'] == pkg['priority']]
            last_prio = [p for p in pkg_dict.values() if p['priority'] == pkg['priority'] - 1]
            if any([all([provider['occurrence'] == pkg['occurrence'], len(current_prio) == 1]),
                    all([pkg['priority'] > 0, len(last_prio) == 0])]):
                continue

            if pkg['priority'] < provider['priority'] + 1:
                pkg['priority'] = provider['priority'] + 1

                increased_prio = True

        return increased_prio

    def _prepare_deps(self, ret):
        """Prepare list for priority calculation"""
        self.logger.error('Recalculate dependencies within current list')
        for pkg in ret.values():
            pkg['real_deps'] = []

            print(f'{floor(list(ret).index(pkg["name"]) * 100 / len(list(ret)))}% done', end='\r')
            for dep in pkg['deps']:
                for provider in ret.values():
                    if dep in provider['provides']:
                        pkg['real_deps'] += [provider['name']]

        # Prepare the arbitrary discriminator to flatten the dependency graph
        self.logger.error('Prepare discriminator for priorities calculation')
        # consider-using-dict-items: We actually want to traverse the dict twice
        # pylint: disable=consider-using-dict-items
        for name in ret:
            print(f'{floor(list(ret).index(name) * 100 / len(list(ret)))}% done', end='\r')
            for pkg in ret.values():
                if name in pkg['real_deps']:
                    ret[name]['occurrence'] += 1
        # pylint: enable= consider-using-dict-items

    def _set_priorities(self, ret):
        """Calculate package priorities based on dependency graph"""
        self._prepare_deps(ret)

        self.logger.error('Setting priorities')
        # Re-calc priority based on a simplified dependency map
        increased_prio = True
        current_pass = 0
        while increased_prio:
            self.logger.info(f'Pass {current_pass}')
            increased_prio = False

            for pkg in ret.values():
                if pkg['priority'] < current_pass:
                    continue

                if self._increase_priority(pkg, ret):
                    increased_prio = True

            current_pass += 1

        # Saving memory
        for pkg in ret.values():
            del pkg['deps']
            del pkg['provides']
            del pkg['occurrence']
            del pkg['real_deps']

        # There still may be gaps in the priority list, so re-compress that.
        current_prio = 0
        prev_prio = 0
        for name, pkg in sorted(ret.items(), key=lambda x: x[1]['priority']):
            if pkg['priority'] > prev_prio:
                current_prio += 1

            prev_prio = pkg['priority']
            ret[name]['priority'] = current_prio

        self.logger.error(f'{current_pass} pass done.')

    def get_reverse_deps(self, packages, depth=0, priorities=True) -> dict:
        """Returns a dict of package reverse dependencies.

        From a list of given packages, request dnf to retrieve the packages that
        require at least one of the elements of this list from its BuildRequires
        field.
        Multiple architectures can be provided, the returned dictionary has an
        "archs" field listing all supported architectures for a dedicated package.

        The priority is informational, package with priority N likely depends on
        packages with priority N-1, unless a circular dependency is encountered.

        Args:
            packages: The list of packages to get reverse dependencies of.
            distrib: The distribution (fedora, epel, centos ...)
            releasever: The release version for the distribution. e.g for fedora:
                36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.
            archs: A set of architectures to look for the packages (x86_64, aarch64 ...).
            depth: The depth of the dependency tree to look for dependencies
                (default: 0, i.e. direct dependencies). Use it with parsimony,
                depth=2 will likely grab the whole distribution.
                e.g.: glibc depth 0: 258 packages, glibc depth 1: 15K packages.

        Returns:
            A dict of packages that depend on the input for their build. The
            dictionary has the following structure:
            ret = {
                'package_name': {
                    'src_type': 'distgit',
                    'priority': N, # N being any number starting from 0
                    'archs': [ ... ]
                    },
                    ...
                }
        """
        # pylint: disable= too-many-arguments, too-many-locals, too-many-branches

        if self.archs is None:
            self.archs = {'x86_64'}

        ret = {}
        current_depth = 0
        pkg_list = [packages]

        if not packages:
            return ret

        while current_depth <= depth:
            pkg_list.append([])
            if depth > 0:
                self.logger.info(
                    f'Level {current_depth} depth for {self.distrib} {self.releasever}'
                )
            for arch in self.archs:
                pkg_dict = self.__get_reverse_deps(pkg_list[current_depth], arch)

                for name, pkg in pkg_dict.items():
                    if name not in ret:
                        ret[name] = {}
                        ret[name]['name'] = name
                        ret[name]['src_type'] = 'distgit'
                        ret[name]['src'] = self.distrib
                        ret[name]['archs'] = pkg['archs']
                        ret[name]['committish'] = '@last_build'
                        # Set default priority
                        ret[name]['priority'] = 0
                        if priorities:
                            ret[name]['deps'] = pkg['deps']
                            ret[name]['provides'] = pkg['provides']
                            ret[name]['occurrence'] = 0

                        pkg_list[current_depth + 1].append(name)

                        self.logger.info(f'Collected data for {len(ret)} packages.')
                        print(f'Collected data for {len(ret)} packages.', end='\r')
                    else:
                        ret[name]['archs'] += pkg['archs']
                        self.logger.info(f'{name} {ret[name]["archs"]}')

            self.logger.error(f'Collected data for {len(ret)} packages.')

            current_depth += 1

        if priorities:
            self._set_priorities(ret)

        return ret

    def group_to_package_list(self, group) -> list:
        """Returns the list of packages contained in a given group

        This is used to convert groups of packages that may be given by the user.
        If the group doesn't exist, then an empty list is returned, and the
        original list will be used.

        Args:
            group: The name of the group to check for
            distrib: The distribution (fedora, epel, centos ...)
            releasever: The release version for the distribution. e.g for fedora:
                36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.

        Returns:
            A list of package names.
        """
        ret = []
        pkgs = []

        # Arch shouldn't matter here
        base = self.dnf_base("x86_64")

        groupid_query = libdnf5.comps.GroupQuery(base)
        groupid_query.filter_groupid(group)
        group_query = libdnf5.comps.GroupQuery(base)
        group_query.filter_name(group)

        group_query |= groupid_query

        if group_query.empty():
            return ret

        group = next(iter(group_query))

        for pkg in group.get_packages():
            # Look for components
            pkgs_tmp = libdnf5.rpm.PackageQuery(base)
            pkgs_tmp.filter_available()
            pkgs_tmp.filter_name(pkg.get_name())
            pkgs_tmp.filter_arch('src')
            pkgs_tmp.filter_latest_evr()

            # Look for providers
            pkgs_tmp_providers = libdnf5.rpm.PackageQuery(base)
            pkgs_tmp_providers.filter_available()
            pkgs_tmp_providers.filter_provides(pkg.get_name())
            pkgs_tmp_providers.filter_arch('src', libdnf5.common.QueryCmp_NEQ)
            pkgs_tmp_providers.filter_latest_evr()

            pkgs_tmp |= pkgs_tmp_providers

            # Prioritize sourcerpm
            pkgs += sorted(list(pkgs_tmp), key=lambda x: (x.get_sourcerpm() is None, x.get_name()))

        for pkg in pkgs:
            name = pkg.get_source_name() or pkg.get_name()

            if name in ret:
                continue

            ret.append(name)

        return ret
