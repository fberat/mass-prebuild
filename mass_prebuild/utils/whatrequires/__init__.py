""" Retrieve reverse dependencies based on a list of packages

    This module provides facilities in order to calculate the reverse
    dependencies of a provided list of packages. The list is segregated based
    on the architecture and the corresponding distribution's package
    repositories that should be looked at.
    The module can either be used stand-alone (as a plain application) or
    imported into another module.
"""
import argparse
from pathlib import Path
import sys
import yaml

import argcomplete

from mass_prebuild import VERSION
from mass_prebuild.backend.log import MpbLog

try:
    from mass_prebuild.utils.whatrequires.dnf5 import MpbWhatrequires
except ImportError:
    from mass_prebuild.utils.whatrequires.dnf4 import MpbWhatrequires


def handle_exception(exc_type, exc_value, exc_traceback):
    """Log exception into a file to ease debugging"""
    if exc_type == SystemExit:
        return

    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger().error('Stopping due to Uncaught exception !')
    logger().logger.critical("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
    log_path = str(Path(Path.home() / ".mpb" / "mpb-whatrequires.log"))
    logger().error(f'Exception log stored in {log_path}')


def logger():
    """Setup and return the logger"""
    if not hasattr(logger, "logger"):
        logger.logger = MpbLog(name='mpb-whatrequires')

    return logger.logger


def main():
    """Calculate reverse dependencies for packages given in command line

    This function is the entry point for the command line interface to
    calculate the reverse dependencies of a given list of packages.

    The list is either printed to STDOUT or to a file, as requested by the
    user.
    """

    parser = argparse.ArgumentParser(
        description='A package reverse dependency collector'
    )

    parser.add_argument(
        '--verbose', '-V', action='store_true', help='increase output verbosity'
    )
    parser.add_argument(
        '--version',
        '-v',
        action='version',
        version=f'%(prog)s {VERSION}',
        help='show the version number and exit',
    )
    parser.add_argument(
        '-o', '--output', default=None, help='where to store the list'
    )

    parser.add_argument(
        '--distrib',
        default='fedora',
        help='distribution for the package repository',
    )

    parser.add_argument('-a', '--arch', default='x86_64', help='architecture')
    parser.add_argument('-d', '--depth', default=0, type=int, help='dependency depth')
    parser.add_argument(
        '-r',
        '--releasever',
        default='rawhide',
        help='release version (e.g 36, rawhide ...)',
    )

    parser.add_argument(
            '-R',
            '--repo',
            help='custom dnf configuration in a special format, see examples/repo.conf.yaml'
    )

    parser.add_argument(
        '--no-deps', action='store_true', help='only retrieve last build info'
    )

    parser.add_argument(
        '--no-priority', action='store_true', help='disable priority calculation'
    )

    parser.add_argument(
        'packages',
        nargs='+',
        type=str,
        action='extend',
        help='list of packages to collect reverse dependencies for',
    )

    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    args.arch = set(args.arch.split())

    logger().level = args.verbose
    sys.excepthook = handle_exception

    if args.verbose:
        pkgs = ', '.join(args.packages)
        logger().error(f'Collecting reverse dependencies for {pkgs}')
        logger().error(f'Result will be provided to {args.output}')

    whatrequires = MpbWhatrequires(logger(), args.distrib, args.releasever, args.arch)

    whatrequires.load_repo_config(args.repo)

    pkg_list = []
    for pkg in args.packages:
        pgroup = whatrequires.group_to_package_list(pkg)
        if pgroup:
            pkg_list += pgroup
        else:
            pkg_list.append(pkg)

    for pkg in pkg_list:
        for arch in args.arch:
            name, committish, nvr, src = whatrequires.get_last_build(pkg, arch)
            logger().error(f'({arch}) {pkg}:{name}:{nvr}:{src} {committish}')

    if args.no_deps:
        return

    pkgs = whatrequires.get_reverse_deps(pkg_list, args.depth, not args.no_priority)

    if args.output:
        with open(args.output, 'w', encoding='utf-8') as file:
            print(f'{yaml.dump(pkgs)}', file=file)
    else:
        logger().error(f'{yaml.dump(pkgs)}')


if __name__ == '__main__':
    main()
