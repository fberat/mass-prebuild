# This is a sample configuration file, listing all the options
# available for the mass pre-builder tool

# archs:[optional] the list of architectures to build for
# default value: x86_64
# accepted values: aarch64, ppc64le, s390x, x86, x86_64
# special value: all
arch: x86_64

# skip_archs:[optional] the list of architectures to skip for reverse
#   dependencies builds
# default value: ''
# accepted values: aarch64, ppc64le, s390x, x86, x86_64

# chroot:[optional] The chroot to build on.
# The values are constructed as [distribution]-[release]. See "Repository
# configuration" in `man mpb.config` to get more details on how these
# `distribution` and `release` keys are interpreted internally.
# default value: fedora-rawhide
# accepted values (where X is a release number): fedora-X, epel-X, centos-stream-X
chroot: fedora-rawhide

# build_id: [optional] The build ID to continue the work on
#   Should not be used for new builds: if the build ID isn't attributed, that
#   will fail.
#  default value: 0
#  accepted value: Any number
#
#  Overridden by '--buildid' or '-b' command line argument
build_id: 0

# name: [optional] The name of the project to be used, may simplify
# identification of the build within the backend infrastructure
# default value: mpb-{build_id}
# accepted value: any string (without spaces)
# WARNING: Re-using the same name for multiple project is possible, and may
# bring confusion (i.e. messing the project in the remote infrastructure)

# stage: [optional] Which stage should the tool start at
#   Is only valid if 'build_id:' is provided
#   default value: 0 or the last known stage if 'build_id:' is provided
#   accepted value: any value between 0 and 4
#
#   Overridden by '--stage' or '-s' command line argument
stage: 0

# packages:[mandatory without positive build ID] the "main" list of packages,
#   that were usually modified and for which you want to test these changes
#   against "reverse dependencies". Can be a yaml list, but is usually a
#   dictionary, which may contain any of the following keys:
# [name of the package]:
#   src_type: The source type, which can be any of: distgit, git, file, url, script
#     default value: distgit
#   src: The source, which depends on the source type
#     For distgit: The name of the owning distribution
#     For git: The git URL
#     For file: The PATH to the file to upload
#     For url: The URL where to find the srpm/spec file
#     For script: The PATH to the script file to be executed
#     default value: distribution extracted from chroot (fedora for the default chroot)
#   committish: The commit ID, tag, or branch to be used when source is git or distgit
#     default value: HEAD of the branch corresponding to 'chroot' (e.g. f36 for
#     fedora-36)
#     special value: '@last_build': ('' are mandatory) Asks Koji to give the commit HASH
#       corresponding to the last NEVRA available to DNF.
#   priority: The build priority relatively to other packages, lower values are build first.
#     Allows to group packages to be built together, and create a dependency tree.
#     This value is internal to mpb, not understood by any builder.
#     default value: 0
#   skip_archs: list of archs to be skipped out of the main list.
#     This is useful when multiple architectures are given but some packages
#     may not build on all of them.
#   retry: How many re-attempts should be made in case of build failure.
#     If set, this overrides the general value (from upper level)
#     default value: 0
#   deps_only: This package is only used to calculate reverse dependencies
#     if this flag is set to True.
#     default value: False
#
# The [name of the package] may be either the main package (e.g. glibc)
# or a sub-package (e.g. glibc-devel). The name of a group of package can
# also be given, but should be prefixed with an '@' character (e.g. '@core').
# In such a case, sub-configuration will be copied to all the members of the
# group.
# If a sub-package name is provided, the reverse dependencies are calculated
# for this sub-package only, and not for all the sub-packages of the
# corresponding main package.
# e.g. if nss_db is provided, reverse dependencies for glibc-utils shouldn't be
# found (unless they are common), yet the complete glibc package will be built.
#
# In the following example, "my-main-package" will be built first from an srpm file.
# Once this build is done, "my-second-package" will be built from distgit,
# while the new version of "my-main-package" may be used as build dependency.
# packages:
#   my-main-package:
#     src_type: file
#     src: /path/to/my/srpm
#     priority: 0
#   my-second-package:
#     priority: 1

# revdeps:[optional] The lists of reverse dependencies to be checked against
#   the main package It is composed of 0 to 2 dictionaries.
#   If this option is not given, the tool automatically calculates the reverse
#   dependencies of 'packages:'.
#   list: The list of dependencies to be used, and thus disable the automatic
#     calculation of them.
#   append: A list of packages to be added on top of the automatically
#     calculated list of dependencies Any value given here that would be already
#     part of the main list of reverse dependencies would override it. That may
#     be used to modify the priorities, or the source for the package (file
#     instead of default distgit).
#   postprocess: (not implemented yet) The path to a post processing script to
#     be used to modify the result of the automatically generated list of reverse
#     dependencies.
#   sample: N
#     Limit the list of reverse dependencies to a sample of `N` elements, taken
#     randomly. This only affect the list if it has been automatically
#     generated, i.e. packages explicitly listed in `append` and `list` are not
#     affected.
#
#   Both 'list:' and 'append:' dictionaries support the same keys as 'packages:'
#
# In the following example, the mass pre-builder will calculate the reverse
# dependencies from "my-main-package" and "my-second-package", then add on top
# of that "my-first-dep" with priority 4, "my-second-dep" with priority 2, and
# all the packages from the Core group with priority 0. For each of the
# packages from the Core group, the tool will ask Koji to give the commit ID of
# the last known successful (and tagged) build.
# The core group will be built first, followed by "my-second-dep", using an
# SRPM available on through an URL (which should be accessible from COPR),
# "my-first-dep" will be built after, using an SRPM provided locally.
# Any of the packages automatically calculated may be built in between.
# revdeps:
#   append:
#     my-first-dep:
#       src_type: file
#       src: /absolute/path/to/srpm
#       priority: 4
#     my-second-dep:
#       src_type: url
#       src: https://blabla/angband.srpm
#       priority: 2
#     '@core':
#       committish: '@last_build'
#   postprocess: /path/to/postprocess/script

# backend:[optional] Which back-end to use to build the packages
#   Each backend may have its own specific configuration
#   default value: copr
#   accepted values: copr, dummy, koji (limited), mock, remote_mock (experimental)
#
#   Overridden by '--backend' command line argument
#   For more details about the available back-ends and their capabilities,
#   please have a look at the `mpb-config(5)` man pages.

# enable_priorities:[optional] Tells the tool to try to arrange builds by
#   priorities
#   default value: False
#   accepted values: True, False
#
#   This option is disabled by default as the priority calculation is far from
#   perfect. The algorithm will probably not find the optimum order, while
#   being slow for huge amount of packages. Yet, for a small amount of packages
#   it may be useful to enable to have a base for package ordering.
enable_priorities: False

# dnf_conf:[optional] Path to a list of URL or metalinks to be used for DNF
#   repositories configuration. See repo.conf.example for details.
#   default value: An empty string
#   accepted values: Any path to an existing file
#
#   Note: if you use the same custom repo.conf for your builds, it may make
#   sense to store the file in '$HOME/.mpb/repo.conf.d'. Configuration file
#   found in this path are considered default configurations and are therefore
#   always loaded.

# clean: [optional] Execute the clean operation NOW, build is skipped
# Overridden by '--clean' command line argument
clean: False

# cancel: [optional] Execute the cancel operation NOW, build is skipped
# Overridden by '--cancel' command line argument
cancel: False

# collect: [optional] Execute the collect operation NOW, build is skipped
# Overridden by '--collect' command line argument
collect: False

# collect_list:[optional] The list of build status that should lead to data
#   collection.
#   default value: failed
#   accepted values: failed, success, running, check_needed, unconfirmed, log
#     control-failed, control-success, control-running, control-check_needed,
#     control-unconfirmed, control-log
#
#   control-* values are meant to collect data from the ".checker" builds
#   *log values are special values that modify the behavior of the other
#     values: (s)rpm files are skipped
#   *failed are special values that include all the "failed" states:
#     check_needed, unconfirmed, failed
#
#   Overridden by '--collect-list' command line argument
collect_list: failed

# config_list_override: [optional] Override the collect_list that was
#   originally saved for 'build_id:' with the new config_list above
#   This is set to True if "--config_list" is passed through the command line
config_list_override: False

# retry: How many re-attempts should be performed in case of package build
#   failure. Applies to all packages unless modified specifically for them.
#   When retry count is decreased, the package is moved down in the priority
#   list. At the end of the mass prebuild, it can be expected that the new
#   priority list generated that way has better chances to succeed with less
#   retries.
#   default value: 0
#   special value: dynamic
#
#   If the value is set to dynamic, the mass pre-builder will rebuild packages
#   on failure. This will stop if there is a gap created in the priority list,
#   which implies that we can't find a build order that will succeed.
#   In such a case, there is no other solution for the user than to provide its
#   own priority list (which may be done either through the 'revdeps:list' or
#   the 'revdeps:append' parameters.
retry: 0

# rebuild: A list of packages to trigger a new build for, or one of the special
#   values. This will decrease the priority of the given packages by one, and
#   fire a new build, which may be useful if there was a cyclic dependency that
#   needed to be broken.
#   default value: None
#   special values: all, failed
#
#   If 'failed' is given, all the packages that are in state 'FAILED',
#   'UNCONFIRMED' or 'CHECK_NEEDED' have their build state and build ID reset,
#   they are then put back in the build queue.
#
#   Main package can't be rebuild this way, as the goal of the mass pre-builder
#   is to have a clean state when main packages are to be rebuilt.
#   If you want to only rebuild failed packages to test a fix of your main
#   packages, prefer to use the mpb-failedconf tool to create a dedicated
#   configuration file (and therefore a dedicated mass pre-build).
#
#   This option implies 'stage: 3'

# verbose: [optional] How much the tool should talk
#   default value: 0
#   accepted values: any number
#
#   Overridden by '--verbose' or '-V' command line argument
#   Verbosity can be increased by specifying the argument multiple times
#   e.g.: mpb -VVV is equivalent to "verbose: 3"
verbose: 0

# copr: [optional] Sub-configuration for the COPR back-end
#  config: Path to the COPR config file to use
#    default value: ~/.config/copr
#    accepted value: any valid path
#  background: which group of package do enable background build for
#    default value: 'revdeps'
#    accepted values: 'all', 'revdeps'
#  disable_background: space separated list of packages to disable background
#                      build for
#     default value: An arbitrary list of packages that are known to take more
#                    than 8h to build if background build is enabled, e.g.:
#                    gcc, chromium ...
#  timeout: The amount of time to wait before considering a package build as
#           failed, in seconds
#     default value: 115200
#     accepted values: any positive integer
#  ownername: The owner of the COPR project that will be created.  If this is
#             not set, the the owner will be 'username' from the copr config
#             file.  This can be used to create COPR projects under a group
#             name rather than a username.
#
#  additional_packages: A list of additional packages to install by default
#                       in each chroot.  Use the 'all' key to specify a package
#                       for all chroots.  Example:
#    all:
#      - gcc
#      - make
#    x86_64-fedora-rawhide:
#      - g++
#
#  with_opts: A list of --with options to add to each chroot.  Use the 'all'
#             key to specify a package for all chroots.  Example:
#    all:
#      - toolchain_clang
#    x86_64-fedora-rawhide:
#      - pgo
#
# On top of that the sub-configuration may contain any of the optional
# parameters of the ProjectProxy.add COPR API:
# https://python-copr.readthedocs.io/en/latest/client_v3/proxies.html#copr.v3.proxies.project.ProjectProxy.add
# Default values are the ones described in this document, with the following
# exceptions: unlisted_on_hp=True, appstream=False.
# You can look at the "New Project" COPR web interface to get more information
# on what values may be provided.
# Some examples:
#  additional_repos:
#    - copr://user/project
#    - http://copr-be.cloud.fedoraproject.org/results/rhughes/f20-gnome-3-12/fedora-$releasever-$basearch/
#  description: "My description that overwrites the default MPB description."

# checker: [optional] Options to be applied specifically on the checker project
#   copr: [optional] Sub-configuration for the COPR back-end
#     The same values as the ones for main copr configuration are accepted
#     By default, if no option is provided, the only element taken from the
#     global configuration is the {copr:config} field. Everything else is set
#     to default.

# remote: [optional] Sub-configuration for the remote mock backend
#   address: The address where the remote is located, may be an IP address or
#            host name.
#
# Note: Remote builds are an experimental feature, that may be heavily modified
# in future releases.
