#!/bin/bash

set -e

npx=$(which npx)
if [ ${npx} == "" ]
then
  echo 'npx is required to execute the changelog script.'
  echo 'On fedora: `dnf install nodejs-npm`'
fi

CHANGELOG=$(dirname $0)/../CHANGELOG.md
line=$(grep -n -m 1 -E "^## " ${CHANGELOG} | cut -d : -f 1)

tail -n +$((line - 1)) ${CHANGELOG} > ${CHANGELOG}.org

npx auto-changelog \
  --template scripts/changelog-template.hbs \
  --hide-credit \
  -l 1000 \
  --prepend \
  -o ${CHANGELOG}.new \
  $@

if [ ! -f ${CHANGELOG}.new ]
then
  rm -f ${CHANGELOG}.org
  exit 0
fi

SUMMARY=$(< $(dirname $0)/summary.txt)

sed -i "s/^-  /- /" ${CHANGELOG}.new
sed -i "s/^- (\([a-z]*\)): /- \1: /" ${CHANGELOG}.new
awk -v r="$SUMMARY" '{gsub(/##SUMMARY##/,r)}1' ${CHANGELOG}.new > ${CHANGELOG}.new.tmp

mv ${CHANGELOG}.new{.tmp,}

cat ${CHANGELOG}.new ${CHANGELOG}.org > ${CHANGELOG}
rm -f ${CHANGELOG}.new ${CHANGELOG}.org
