#!/bin/bash

set -e

npx=$(which npx)
if [ ${npx} == "" ]
then
  echo 'npx is required to execute the changelog script.'
  echo 'On fedora: `dnf install nodejs-npm`'
fi

if [ $# -lt 1 ]
then
  echo "New version should be provided e.g.: v1.2.3"
  exit 1
fi

if [[ ! $1 =~ ^[vV]([0-9]+)\.([0-9]+)\.([0-9]+) ]]
then
  echo "Given new version should be like v1.2.3"
  exit 2
fi

MAJOR=${BASH_REMATCH[1]}
MINOR=${BASH_REMATCH[2]}
REVISION=${BASH_REMATCH[3]}
tag="v${MAJOR}.${MINOR}.${REVISION}"

git tag -a -m "Release ${tag}" ${tag}

$(dirname $0)/changelog.sh --starting-version ${tag}

git commit -as \
  -m "docs: Update CHANGELOG with release details" \
  -m "" \
  -m "Added release information for ${tag} to the CHANGELOG."

sed -i "s/VERSION_INFO = .*/VERSION_INFO = (${MAJOR}, ${MINOR}, ${REVISION})/" \
  $(dirname $0)/../mass_prebuild/__init__.py

git commit -as \
  -m "release: Version ${MAJOR}.${MINOR}.${REVISION}" \
  -m "" \
  -m "Bump version."

git tag -d ${tag}
